<?php

return [
    //guard => name
    'super-admin' => [
        'guard'       => null,
        'permissions' => null
    ],
    'admin'       => [
        'guard'       => null,
        'permissions' => [
            'auctions'                => ['c', 'r', 'u'],
            'auction.school-families' => '*',
            'auction.vendors'         => '*',
            'auction.donations'       => '*',
            'auction.auction-items'   => '*',
            'donors'                  => '*',
            'school-families'         => '*',
            'vendors'                 => '*',
            'users'                   => '*',
            'invites'                 => '*',
        ]
    ],
    'coordinator' => [
        'guard'       => null,
        'permissions' => [
            'auctions'                => ['r', 'u'],
            'auction.school-families' => ['r', 'register'],
            'auction.vendors'         => ['r', 'register', 'update-status'],
            'auction.donations'       => ['c', 'r', 'u', 'd'],
            'auction.auction-items'   => ['c', 'r', 'u', 'd', 'reorder'],
            'donors'                  => ['c', 'r', 'u'],
            'school-families'         => ['c', 'r', 'u'],
            'vendors'                 => ['c', 'r', 'u'],
        ],
    ],
    'parent'      => [
        'guard' => null
    ],
    'principal'   => [
        'guard'       => null,
        'permissions' => [
            'auctions'                => ['r'],
            'auction.school-families' => ['r'],
            'auction.vendors'         => ['r'],
            'auction.donations'       => ['r', 'approve'],
            'auction.auction-items'   => ['r'],
            'donors'                  => ['r'],
            'school-families'         => ['r'],
            'vendors'                 => ['r'],
        ],
    ]
];
