<?php

return [
    'resources' => [
        'auctions'                => [
            'crud' => ['c', 'r', 'u', 'd'],
        ],
        'auction.school-families' => [
            'crud'   => ['r'],
            'custom' => [
                'register',
            ]
        ],
        'auction.vendors'         => [
            'crud'   => ['r'],
            'custom' => [
                'register',
                'update-status'
            ]
        ],
        'auction.donations'       => [
            'crud'   => ['c', 'r', 'u', 'd'],
            'custom' => [
                'approve'
            ]
        ],
        'auction.auction-items'   => [
            'crud'   => ['c', 'r', 'u', 'd'],
            'custom' => [
                'reorder'
            ]
        ],
        'donors'                  => [
            'crud' => ['c', 'r', 'u', 'd'],
        ],
        'school-families'         => [
            'crud' => ['c', 'r', 'u', 'd'],
        ],
        'vendors'                 => [
            'crud' => ['c', 'r', 'u', 'd'],
        ],
        'users'                   => [
            'crud' => ['r', 'u']
        ],
        'invites'                 => [
            'crud'   => ['c', 'r', 'u', 'd'],
            'custom' => [
                'resend'
            ]
        ]
    ]
];
