<?php


namespace App\FileImporters;


use App\Models\FileImport;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use League\Csv\Reader;
use Storage;

abstract class BaseFileImporter
{
    /**
     * @var bool
     */
    protected $build_metadata = false;

    /**
     * @var Reader
     */
    private $file_reader;

    /**
     * @return string
     */
    abstract protected function getUniqueColumn(): string;

    /**
     * @param array $data
     * @return array
     */
    abstract protected function mapRecord(array $data): array;

    /**
     * @param FileImport $import
     * @throws \League\Csv\Exception
     */
    public function import(FileImport $import)
    {
        if (!is_file($import->full_file_path)) {
            throw new Exception("File not found: {$import->full_file_path}");
        }

        $this->_prepCsvFile($import);

        if ($this->build_metadata) {
            $this->getImportMetadata($import);
        }

        foreach ($this->file_reader->getRecords() as $record) {
            $model_data   = $this->mapRecord($record);
            $model_record = $this->findRecord($import, $model_data);

            if (!$model_record) {
                $model_class_name = $this->_getModel($import);
                $model_record     = new $model_class_name;
            }

            $model_record->fill($model_data)->save();
        }

        $import->imported_at = Carbon::now();
        $import->save();
    }

    /**
     * @return $this
     */
    public function buildMetadata()
    {
        $this->build_metadata = true;

        return $this;
    }

    /**
     * @param FileImport $import
     */
    protected function getImportMetadata(FileImport $import): void
    {
        $records = $this->file_reader->count();

        $import->records = $records;
        $import->save();
    }

    /**
     * Caller can override this to specify more customized functionality
     *
     * @param FileImport $import
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    protected function findRecord(FileImport $import, array $data)
    {
        $key_value = $data[$this->getUniqueColumn()] ?? null;
        /** @var Model $model_class */
        $model_class = $this->_getModel($import);

        return $model_class::where($this->getUniqueColumn(), 'like', "%$key_value%")->first();
    }

    /**
     * @param FileImport $import
     * @return string
     * @throws Exception
     */
    private function _getModel(FileImport $import)
    {
        $model = Relation::getMorphedModel($import->type);

        if (is_null($model)) {
            throw new Exception('No class found: ' . $import->type);
        }

        return $model;
    }

    /**
     * @param FileImport $import
     * @throws \League\Csv\Exception
     */
    private function _prepCsvFile(FileImport $import): void
    {
        $this->file_reader = Reader::createFromPath($import->full_file_path);
        $this->file_reader->setHeaderOffset(0);
    }
}