<?php


namespace App\FileImporters;


use App\Services\StatesService;

class VendorImporter extends BaseFileImporter
{
    /**
     * @var bool
     */
    protected $build_metadata = true;

    /**
     * @return string
     */
    protected function getUniqueColumn(): string
    {
        return 'name';
    }

    /**
     * @param array $data
     * @return array
     */
    protected function mapRecord(array $data): array
    {
        return [
            'name'         => $data['Vendor'],
            'contact_name' => $data['Contact'],
            'phone'        => $data['Phone'],
            'address'      => $data['Address'],
            'city'         => $this->_getCity($data),
            'state'        => $this->_getState($data),
            'zip'          => $this->_getZip($data)
        ];
    }

    /**
     * @param array $row
     * @return string|null
     */
    private function _getCity(array $row)
    {
        $parts = array_map('trim', explode(',', $row['Locality'], 2));

        if (count($parts) < 1 || empty($parts[0])) {
            return null;
        }

        return $parts[0];
    }

    /**
     * @param array $row
     * @return string|null
     */
    private function _getState(array $row)
    {
        $parts = array_map('trim', explode(',', $row['Locality'], 2));

        if (count($parts) < 1 || empty($parts[0]) || empty($parts[1])) {
            return null;
        }

        $last = array_map('trim', explode(' ', $parts[1], 2));

        if (count($last) < 1 || empty($last[0])) {
            return null;
        }

        $state = $last[0];

        if(strlen($state) < 1) {
            return null;
        } else if (strlen($state) < 3) {
            return strtoupper($state);
        } else {
            $state_service = new StatesService();

            return $state_service->getCodeByName($state);
        }
    }

    /**
     * @param array $row
     * @return |null
     */
    private function _getZip(array $row)
    {
        $parts = array_map('trim', explode(',', $row['Locality'], 2));

        if (count($parts) < 1 || empty($parts[0]) || empty($parts[1])) {
            return null;
        }

        $last = array_map('trim', explode(' ', $parts[1], 2));

        if (count($last) < 2 || empty($last[0]) || empty($last[1])) {
            return null;
        }

        return $last[1];
    }
}