<?php

namespace App\Providers;

use App\Models\Auction;
use App\Models\Donor;
use App\Models\User;
use App\Models\Vendor;
use App\Observers\AuctionObserver;
use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() == 'local') {
            $this->app->register(IdeHelperServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            'donor'  => Donor::class,
            'vendor' => Vendor::class,
            'user'   => User::class,
        ]);

        Auction::observe(AuctionObserver::class);
    }
}
