<?php


namespace App\Providers;


use App\Http\View\Composers\VendorStatusFiltersComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    public function boot()
    {
        View::composer(['auctions.vendors.index'], VendorStatusFiltersComposer::class);
    }
}
