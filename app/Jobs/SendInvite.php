<?php

namespace App\Jobs;

use App\Mail\NewInvite;
use App\Models\Invite;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendInvite implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Invite
     */
    private $invite;

    /**
     * Create a new job instance.
     *
     * @param Invite $invite
     */
    public function __construct(Invite $invite)
    {
        $this->invite = $invite;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->invite->update([
            'invite_sent_at' => null
        ]);

        $mail = new NewInvite($this->invite);

        Mail::send($mail);

        $this->invite->update([
            'invite_sent_at' => Carbon::now()
        ]);
    }
}
