<?php

namespace App\Rules;

use App\Models\Auction;
use App\Models\AuctionItem;
use App\Models\Donation;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Database\Eloquent\Builder;

class ItemDonationWithQtyNeedsQty implements Rule
{
    /**
     * @var array
     */
    private $donations;

    /**
     * @var
     */
    private $max_qty;

    /**
     * @var
     */
    private $auction_item_id;

    /**
     * ItemDonationWithQtyNeedsQty constructor.
     * @param array $donations
     * @param $auction_item_id
     */
    public function __construct(array $donations = [], $auction_item_id = null)
    {
        $this->donations       = $donations;
        $this->auction_item_id = $auction_item_id;
    }


    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        /** @var Auction $auction */
        $auction = request()->route('auction');
        $index   = collect(explode('.', $attribute, 2))->last();
        $id      = $this->donations[$index];
        /** @var Donation $donation */
        $donation      = Donation::find($id);
        $this->max_qty = $donation->getAvailableQtyForAuction($auction, !is_null($this->auction_item_id) ? [$this->auction_item_id] : []);

        #donation does not have a qty set so no qty needs entered
        if (is_null($donation->qty))
            return true;

        #donation has set qty so item must be a subset of that qty
        if (is_null($value))
            return false;

        return $value <= $this->max_qty;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Qty must be greater than 0 and equal to or less than ' . $this->max_qty;
    }
}
