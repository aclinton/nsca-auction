<?php

namespace App\Rules;

use App\Models\Auction;
use App\Models\AuctionItem;
use Illuminate\Contracts\Validation\Rule;

class UniqueItemAuctionNumber implements Rule
{
    private $ignore_number;

    public function __construct($ignore_number = null)
    {
        $this->ignore_number = $ignore_number;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        /** @var Auction $auction */
        $auction   = request()->route('auction');
        $check_val = $this->ignore_number != $value;

        if ($check_val) {
            return !AuctionItem::whereAuctionId($auction->id)->where('item_number', $value)->exists();
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'An item with this number already exists.';
    }
}
