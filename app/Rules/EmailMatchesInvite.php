<?php

namespace App\Rules;

use App\Models\Invite;
use Illuminate\Contracts\Validation\Rule;

class EmailMatchesInvite implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $token  = request()->route('token');
        $invite = Invite::whereToken($token)->first();

        return $invite->email == $value;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The supplied :attribute does not match the :attribute on the invite.';
    }
}
