<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class PreventDuplicateItemDonations implements Rule
{
    /**
     * @var array
     */
    private $donation_ids;

    /**
     * Create a new rule instance.
     *
     * @param array $donation_ids
     */
    public function __construct(array $donation_ids)
    {
        $this->donation_ids = $donation_ids;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $count = collect($this->donation_ids)->filter(function ($id) use($value) {
            return $id == $value;
        })->count();

        return $count === 1;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Cannot have multiple instances of the same donation in an auction item.';
    }
}
