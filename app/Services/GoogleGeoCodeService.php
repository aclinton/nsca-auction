<?php


namespace App\Services;


use GuzzleHttp\Client;

class GoogleGeoCodeService
{
    /**
     * @var Client
     */
    private $client;

    /**
     * GoogleGeoLocationService constructor.
     */
    public function __construct()
    {
        $this->client = new Client([
            'base_uri'   => 'https://maps.googleapis.com/maps/api/geocode/json',
            'exceptions' => false
        ]);
    }

    public function geocode(string $address = '')
    {
        $res = $this->client->get('', [
            'query' => [
                'key'     => config('services.google_geocoding.key.key'),
                'address' => $address
            ]
        ]);

        $status = $res->getStatusCode();

        if($status >= 200 && $status < 300) {

        }
    }
}