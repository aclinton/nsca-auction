<?php


namespace App\Services;


class PermissionService
{
    /**
     * @var array
     */
    protected static $map = [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ];

    /**
     * @param string $resource
     * @return array
     * @throws \Exception
     */
    public static function getResourcePermissions(string $resource)
    {
        $config = config('permissions.resources');
        $config = $config[$resource] ?? null;

        if (is_null($config)) {
            throw new \Exception("Permissions resource $resource does not exist");
        }

        foreach ($config['crud'] ?? [] as $perm) {
            $permissions[] = self::$map[$perm];
        }

        foreach ($config['custom'] ?? [] as $perm) {
            $permissions[] = $perm;
        }

        return collect($permissions ?? [])->map(function ($p) use($resource) {
            return "$resource.$p";
        })->values()->all();
    }

    /**
     * @param string $resource
     * @param string $abbr
     * @return mixed|null
     * @throws \Exception
     */
    public static function getResourceAction(string $resource, string $abbr)
    {
        $config = config('permissions.resources');
        $config = $config[$resource] ?? null;
        $mapped = self::$map[$abbr] ?? null;

        if (is_null($mapped)) {
            if(!in_array($abbr, $config['custom'] ?? [])) {
                throw new \Exception("Action could not be found for $resource => " . $abbr);
            }

            $mapped = $abbr;
        }

        return "$resource.$mapped";
    }
}
