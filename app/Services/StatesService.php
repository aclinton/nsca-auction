<?php


namespace App\Services;


class StatesService
{
    /**
     * @var \Illuminate\Support\Collection
     */
    private $states;

    /**
     * StatesService constructor.
     */
    public function __construct()
    {
        $this->states = collect(config('states'));
    }

    /**
     * @param string $state_name
     * @return false|int|string
     */
    public function getCodeByName(string $state_name)
    {
        $key = array_search(ucfirst($state_name), $this->states->all()) ?? null;

        if($key) {
            return $key;
        }

        return null;
    }
}