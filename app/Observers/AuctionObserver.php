<?php


namespace App\Observers;


use App\Models\Auction;
use Illuminate\Support\Str;

class AuctionObserver
{
    public function created(Auction $auction)
    {
        $auction->code = $this->_getUniqueCode();
        $auction->save();
    }

    public function updated(Auction $auction)
    {
        if(empty($auction->code)) {
            $auction->code = $this->_getUniqueCode();
            $auction->save();
        }
    }

    /**
     * @return string
     */
    private function _getUniqueCode()
    {
        $code = Str::random(8);

        while (Auction::whereCode($code)->exists()) {
            $code = Str::random(8);
        }

        return $code;
    }
}
