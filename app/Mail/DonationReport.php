<?php

namespace App\Mail;

use App\Models\Auction;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DonationReport extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var string
     */
    private $report_path;

    /**
     * @var Auction
     */
    public $auction;

    /**
     * Create a new message instance.
     *
     * @param Auction $auction
     * @param string $report_path
     */
    public function __construct(Auction $auction, string $report_path)
    {
        $this->report_path = $report_path;
        $this->auction = $auction;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.report-donations')
            ->subject($this->auction->year . " Donation Report")
            ->attach($this->report_path, [
                'as'   => 'donations-report.csv',
                'mime' => 'application/csv'
            ]);
    }
}
