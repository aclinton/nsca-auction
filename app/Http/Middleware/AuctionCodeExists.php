<?php

namespace App\Http\Middleware;

use App\Models\Auction;
use Closure;

class AuctionCodeExists
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $code = $request->route('auction_code');

        if(!Auction::whereCode($code)->exists()) {
            abort(404);
        }

        return $next($request);
    }
}
