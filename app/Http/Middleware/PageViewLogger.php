<?php

namespace App\Http\Middleware;

use Closure;

class PageViewLogger
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        activity()->causedBy($request->user())
            ->withProperties([
                'url'    => $request->getUri(),
                'method' => $request->getMethod(),
                'ip'     => $request->getClientIp()
            ])
            ->log('action');

        return $next($request);
    }
}
