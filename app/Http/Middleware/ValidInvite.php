<?php

namespace App\Http\Middleware;

use App\Models\Invite;
use Closure;

class ValidInvite
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->route('token');

        if(empty($token)) {
            return abort(404);
        }

        $invite = Invite::whereToken($token)->first();

        if(!$invite) {
            return redirect()->route('auctions.index')->with('info', 'No invite with matching token found in the system.');
        }

        if($invite->accepted) {
            return redirect()->route('auctions.index')->with('info', 'Invite has already been accepted');
        }

        return $next($request);
    }
}
