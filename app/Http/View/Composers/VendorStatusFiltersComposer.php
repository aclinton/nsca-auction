<?php


namespace App\Http\View\Composers;


use App\Http\Resources\VendorStatus as VendorStatusResource;
use App\Models\VendorStatus;
use Illuminate\View\View;

class VendorStatusFiltersComposer
{
    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $statuses      = VendorStatus::whereNotIn('name', [
            'approved',
            'rejected'
        ])->orderBy('name')->get();
        $statuses_json = VendorStatusResource::collection($statuses)->jsonSerialize();

        $view->with('vendor_statuses', $statuses);
        $view->with('vendor_statuses_json', $statuses_json);
    }
}
