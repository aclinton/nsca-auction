<?php

namespace App\Http\Controllers;

use App\Models\Auction;
use App\Models\Vendor;

class PublicAuctionVendorsController extends Controller
{
    /**
     * @param $auction_code
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke($auction_code)
    {
        $auction = Auction::whereCode($auction_code)->first();

        return view('auctions.public.vendors')->with(compact('auction'));
    }
}
