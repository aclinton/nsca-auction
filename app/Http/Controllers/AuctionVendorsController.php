<?php

namespace App\Http\Controllers;


use App\Http\Resources\Auction as AuctionResource;
use App\Http\Resources\Vendor as VendorResource;
use App\Http\Resources\VendorStatus;
use App\Models\Auction;
use App\Models\SchoolFamily;
use App\Models\Vendor;

class AuctionVendorsController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:auction.vendors.read')->only('index');
    }

    /**
     * @param Auction $auction
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Auction $auction)
    {
        $auction->load('donations.memberDonors', 'donations.vendorDonors');
        $unregistered_vendors = Vendor::notRegisteredToAuction($auction)->get();
        $school_families      = SchoolFamily::orderBy('family_last_name')->get();
        $vendors              = $auction->vendors()->with('pivot.schoolFamily', 'pivot.status')->get();

        $json = [
            'auction'         => AuctionResource::make($auction)->jsonSerialize(),
            'vendors'         => VendorResource::collection($vendors)->jsonSerialize(),
            'school_families' => \App\Http\Resources\SchoolFamily::collection($school_families)->jsonSerialize()
        ];

        return view('auctions.vendors.index', compact('auction', 'unregistered_vendors', 'school_families', 'json'));
    }
}
