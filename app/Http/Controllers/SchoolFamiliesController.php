<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateNewSchoolFamilyRequest;
use App\Http\Requests\UpdateSchoolFamilyRequest;
use App\Models\SchoolFamily;

class SchoolFamiliesController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:school-families.read')->only('index');
        $this->middleware('can:school-families.create')->only('create', 'store');
        $this->middleware('can:school-families.update')->only('edit', 'update');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $families = SchoolFamily::orderBy('family_last_name')->orderBy('first_parent_name')->get();

        return view('school-families.index', compact('families'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('school-families.create');
    }

    /**
     * @param CreateNewSchoolFamilyRequest $req
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateNewSchoolFamilyRequest $req)
    {
        $family = SchoolFamily::create($req->all());

        return redirect()->route('school-families.index')->with('success', 'Created new family ' . $family->family_last_name);
    }

    /**
     * @param SchoolFamily $family
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(SchoolFamily $school_family)
    {
        return view('school-families.edit')->with(['family' => $school_family]);
    }

    /**
     * @param UpdateSchoolFamilyRequest $req
     * @param SchoolFamily $school_family
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateSchoolFamilyRequest $req, SchoolFamily $school_family)
    {
        $school_family->update($req->all());

        return redirect()->route('school-families.index')->with('success', 'Updated ' . $school_family->family_last_name . ' family');
    }
}
