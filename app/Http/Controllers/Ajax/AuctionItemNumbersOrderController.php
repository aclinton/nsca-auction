<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use App\Http\Resources\AuctionItemCollection;
use App\Models\Auction;
use App\Models\AuctionItem;
use Illuminate\Support\Facades\DB;

class AuctionItemNumbersOrderController extends Controller
{
    /**
     * @param Auction $auction
     * @return AuctionItemCollection
     */
    public function __invoke(Auction $auction)
    {
        $id_list = request()->get('ids', []);

        DB::beginTransaction();

        collect($id_list)->each(function ($id, $index) {
            $number = $index + 1;
            AuctionItem::where('id', $id)->update([
                'item_number' => $number
            ]);
        });

        DB::commit();

        return AuctionItemCollection::make($auction->auctionItems()->get());
    }
}
