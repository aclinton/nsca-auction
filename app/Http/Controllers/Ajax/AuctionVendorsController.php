<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateAuctionVendorRequest;
use App\Models\Auction;
use App\Models\Vendor;
use Illuminate\Http\Request;

class AuctionVendorsController extends Controller
{
    /**
     * @param Auction $auction
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Auction $auction)
    {
        $vendors = $auction->vendors()->with('pivot.schoolFamily', 'pivot.status')->get();

        return \App\Http\Resources\Vendor::collection($vendors);
    }

    /**
     * @param Auction $auction
     * @param Vendor $vendor
     * @param UpdateAuctionVendorRequest $req
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Auction $auction, Vendor $vendor, UpdateAuctionVendorRequest $req)
    {
        $data = [
            'vendor_status_id' => $req->get('status'),
            'school_family_id' => $req->get('family')
        ];
        $auction->vendors()->updateExistingPivot($vendor->id, $data);

        return response()->json(['data' => [], 'success' => true]);
    }
}
