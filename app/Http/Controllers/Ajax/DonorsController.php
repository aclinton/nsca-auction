<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateDonorRequest;
use App\Http\Resources\Donor as DonorResource;
use App\Http\Resources\DonorCollection;
use App\Models\Donor;

class DonorsController extends Controller
{
    /**
     * @return DonorCollection
     */
    public function index()
    {
        $donors = Donor::orderBy('name')->get();

        return new DonorCollection($donors);
    }

    /**
     * @param CreateDonorRequest $req
     * @return DonorResource
     */
    public function store(CreateDonorRequest $req)
    {
        $donor = Donor::create($req->all());

        return new DonorResource($donor);
    }
}
