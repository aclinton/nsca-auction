<?php

namespace App\Http\Controllers\Ajax;

use App\Actions\RegisterAuctionVendors;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateAuctionVendorRegistrationsRequest;
use App\Models\Auction;

class AuctionVendorRegistrationsController extends Controller
{
    /**
     * @param CreateAuctionVendorRegistrationsRequest $req
     * @param Auction $auction
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateAuctionVendorRegistrationsRequest $req, Auction $auction)
    {
        $action = new RegisterAuctionVendors($auction, $req->get('vendor_ids'));
        $action->execute();

        return response()->json(['success' => true, 'data' => []], 200);
    }
}
