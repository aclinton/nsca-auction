<?php


namespace App\Http\Controllers\Ajax;


use App\Http\Controllers\Controller;
use App\Http\Resources\AuctionItemCollection;
use App\Models\Auction;

class AuctionItemsController extends Controller
{
    /**
     * @param Auction $auction
     * @return AuctionItemCollection
     */
    public function index(Auction $auction)
    {
        $items = $auction->auctionItems()->with('donations.vendorDonors', 'donations.memberDonors')->orderBy('item_number')->get();

        return AuctionItemCollection::make($items);
    }
}
