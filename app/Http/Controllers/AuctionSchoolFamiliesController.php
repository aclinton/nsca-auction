<?php

namespace App\Http\Controllers;

use App\Models\Auction;

class AuctionSchoolFamiliesController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:auction.school-families.read')->only('index');
    }

    /**
     * @param Auction $auction
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Auction $auction)
    {
        $auction->load([
            'schoolFamilies.auctionVendors' => function ($q) use ($auction) {
                $q->where('auction_id', $auction->id);
            },
            'schoolFamilies.auctionVendors.vendor',
            'schoolFamilies.auctionVendors.status',
            'schoolFamilies'                => function ($query) {
                $query->orderBy('family_last_name');
            }]);

        return view('auctions.school-families.index')->with(compact('auction'));
    }
}
