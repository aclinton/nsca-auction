<?php

namespace App\Http\Controllers;

use App\Actions\VendorActions;
use App\Http\Requests\CreateVendorRequest;
use App\Http\Requests\UpdateVendorRequest;
use App\Models\Vendor;

class VendorsController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:vendors.read')->only('index');
        $this->middleware('can:vendors.create')->only('create', 'store');
        $this->middleware('can:vendors.update')->only('edit', 'update');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $vendors = Vendor::orderBy('name')->get();

        return view('vendors.index')->with(compact('vendors'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('vendors.create');
    }

    /**
     * @param CreateVendorRequest $request
     * @param VendorActions $actions
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateVendorRequest $request, VendorActions $actions)
    {
        $vendor = $actions->create($request);

        return redirect()->route('vendors.index')->with('success', "Created a new vendor: {$vendor->name}");
    }

    /**
     * @param Vendor $vendor
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Vendor $vendor)
    {
        return view('vendors.edit', compact('vendor'));
    }

    /**
     * @param UpdateVendorRequest $req
     * @param Vendor $vendor
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateVendorRequest $req, Vendor $vendor)
    {
        $vendor->update($req->all());

        if (!$req->has('discontinued')) {
            $vendor->update([
                'discontinued' => false
            ]);
        }

        if ($req->has('redirect')) {
            return redirect($req->get('redirect'))->with('success', "Updated {$vendor->name}");
        }

        return redirect()->route('vendors.index')
            ->with('success', "Updated {$vendor->name}");
    }
}
