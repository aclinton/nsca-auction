<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAuctionRequest;
use App\Models\Auction;
use App\Models\SchoolFamily;
use App\Models\Vendor;
use App\Models\VendorStatus;

class AuctionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:auctions.create')->only('create', 'store');
        $this->middleware('can:auctions.read')->only('index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $auctions = Auction::withCount('participatingVendors')
            ->withCount('donations')
            ->withCount('silentAuctionItems')
            ->withCount('liveAuctionItems')
            ->orderBy('year')
            ->get();

        return view('auctions.index')->with(compact('auctions'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('auctions.create');
    }

    /**
     * @param CreateAuctionRequest $req
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateAuctionRequest $req)
    {
        $data = array_merge($req->all(), [
            'code' => Auction::getNewCode()
        ]);

        $auction = Auction::create($data);

        return redirect()->route('auctions.index')->with('success', 'Created auction for year ' . $auction->year);
    }
}
