<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Cache;
use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;

class ActivitiesController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:super-admin')->only('index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $page = request()->get('page', 1);
        $key  = 'activities:' . md5($page);

        $activities = Cache::remember($key, 5, function () {
            return Activity::orderByDesc('created_at')->paginate(25);
        });

        return view('admin.activities.index')->with(compact('activities'));
    }
}
