<?php

namespace App\Http\Controllers\Admin;

use App\Actions\SendNewEmailInvite;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateNewInviteRequest;
use App\Models\Invite;
use Spatie\Permission\Models\Role;

class InvitesController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:invites.read')->only('index');
        $this->middleware('can:invites.create')->only('create', 'store');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $invites = Invite::orderBy('created_at')->get();

        return view('admin.invites.index')->with(compact('invites'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $roles = Role::orderBy('name')->get();

        return view('admin.invites.create')->with(compact('roles'));
    }

    /**
     * @param CreateNewInviteRequest $req
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateNewInviteRequest $req)
    {
        $action = new SendNewEmailInvite($req->get('email'), $req->get('roles', []));
        $action->execute();

        return redirect()->route('admin.invites.index')->with('success', 'Invite sent to ' . $req->get('email'));
    }
}
