<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateUserProfileRequest;
use App\Models\User;
use Spatie\Permission\Models\Role;

class UsersController extends Controller
{
    /**
     * UsersController constructor.
     */
    public function __construct()
    {
        $this->middleware(['can:users.read'])->only('index');
        $this->middleware(['can:users.update'])->only('edit', 'update');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = User::orderBy('name')->get();

        return view('admin.users.index')->with(compact('users'));
    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(User $user)
    {
        $roles = Role::orderBy('name')->get();

        return view('admin.users.edit')->with(compact('user', 'roles'));
    }

    /**
     * @param UpdateUserProfileRequest $req
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateUserProfileRequest $req, User $user)
    {
        $user->update([
            'name'  => $req->get('name'),
            'email' => $req->get('email')
        ]);

        if($req->has('roles')) {
            $user->syncRoles($req->get('roles'));
        }

        return redirect()->route('admin.users.index')->with('success', 'Updated ' . $user->name);
    }
}
