<?php


namespace App\Http\Controllers\Admin;


use App\Actions\ResendInvitation;
use App\Http\Controllers\Controller;
use App\Models\Invite;

class SendInvitesController extends Controller
{
    public function __invoke(Invite $invite)
    {
        $action = new ResendInvitation($invite);
        $action->execute();

        return redirect()->route('admin.invites.index')->with('success', 'Resent invite to ' . $invite->email);
    }
}
