<?php

namespace App\Http\Controllers;

use App\Models\Donation;

class AuctionDonationRejectsController extends Controller
{
    public function __invoke(Donation $donation)
    {
        if($donation->rejected) {
            $message = "{$donation->name} has already been rejected";
        } else {
            $donation->update(['accepted' => 0]);
            $message = "{$donation->name} successfully rejected";
        }

        return redirect()->back()->with('success', $message);
    }
}
