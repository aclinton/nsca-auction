<?php

namespace App\Http\Controllers;

use App\Models\Donation;

class AuctionDonationApprovalsController extends Controller
{
    public function __invoke(Donation $donation)
    {
        if($donation->accepted) {
            $message = "{$donation->name} has already been approved";
        } else {
            $donation->update(['accepted' => 1]);
            $message = "{$donation->name} successfully approved";
        }

        return redirect()->back()->with('success', $message);
    }
}
