<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDonorRequest;
use App\Http\Requests\UpdateDonorRequest;
use App\Models\Donor;

class DonorsController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:donors.read')->only('index');
        $this->middleware('can:donors.create')->only('create', 'store');
        $this->middleware('can:donors.update')->only('edit', 'update');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $donors = Donor::orderBy('name')->get();

        return view('donors.index', compact('donors'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('donors.create');
    }

    /**
     * @param CreateDonorRequest $req
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateDonorRequest $req)
    {
        $donor = Donor::create($req->all());

        return redirect()->route('donors.index')->with('success', 'Donor created: ' . $donor->name);
    }

    /**
     * @param Donor $donor
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Donor $donor)
    {
        return view('donors.edit')->with(compact('donor'));
    }

    /**
     * @param UpdateDonorRequest $req
     * @param Donor $donor
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateDonorRequest $req, Donor $donor)
    {
        $donor->update($req->all());

        return redirect()->route('donors.index')->with('success', 'Donor updated: ' . $donor->name);
    }
}
