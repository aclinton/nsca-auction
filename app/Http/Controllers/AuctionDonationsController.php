<?php

namespace App\Http\Controllers;

use App\Actions\CreateAuctionDonation;
use App\Actions\UpdateAuctionDonation;
use App\Http\Requests\CreateAuctionDonationRequest;
use App\Http\Requests\UpdateAuctionDonationRequest;
use App\Http\Resources\Donor as DonorResource;
use App\Http\Resources\DonorCollection;
use App\Models\Auction;
use App\Models\Donation;
use App\Models\DonationType;
use App\Models\Donor;

class AuctionDonationsController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:auction.donations.read')->only('index');
        $this->middleware('can:auction.donations.create')->only('create', 'store');
        $this->middleware('can:auction.donations.update')->only('edit', 'update');
    }

    /**
     * @param Auction $auction
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Auction $auction)
    {
        return view('auctions.donations.index')->with(compact('auction'));
    }

    /**
     * @param Auction $auction
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Auction $auction)
    {
        $donation_types = DonationType::orderBy('name')->get();
        $donors         = Donor::orderBy('name')->get();
        $json           = [
            'donors' => DonorCollection::make($donors)->jsonSerialize()
        ];

        return view('auctions.donations.create')
            ->with(compact('auction', 'donation_types', 'json'));
    }

    /**
     * @param Auction $auction
     * @param CreateAuctionDonationRequest $req
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Auction $auction, CreateAuctionDonationRequest $req)
    {
        $action = new CreateAuctionDonation($req->all(), $auction);
        $action->execute();

        return redirect()->route('auctions.donations.index', $auction)->with('success', 'Created new donation!');
    }

    /**
     * @param Auction $auction
     * @param Donation $donation
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Auction $auction, Donation $donation)
    {
        $donation_types = DonationType::orderBy('name')->get();
        $donors         = Donor::orderBy('name')->get();
        $json           = [
            'donors'           => DonorCollection::make($donors)->jsonSerialize(),
            'selected_members' => DonorResource::collection($donation->memberDonors)->jsonSerialize()
        ];

        if ($donation->memberDonors->count()) {
            $donor_type = 'member';
        } else if ($donation->vendorDonors->count()) {
            $donor_type = 'vendor';
        }

        return view('auctions.donations.edit')->with(compact('auction', 'donation', 'donor_type', 'json', 'donation_types'));
    }

    /**
     * @param UpdateAuctionDonationRequest $req
     * @param Auction $auction
     * @param Donation $donation
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateAuctionDonationRequest $req, Auction $auction, Donation $donation)
    {
        $action = new UpdateAuctionDonation($req->all(), $donation);
        $action->execute();

        return redirect()->route('auctions.donations.index', $auction)
            ->with('success', 'Updated ' . $donation->name);
    }
}
