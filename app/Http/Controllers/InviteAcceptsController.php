<?php

namespace App\Http\Controllers;

use App\Http\Requests\InviteAcceptsRequest;
use App\Models\Invite;
use App\Models\User;

class InviteAcceptsController extends Controller
{
    /**
     * @param $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create($token)
    {
        $invite = Invite::whereToken($token)->first();

        return view('invites.accept')->with(compact('invite'));
    }

    /**
     * @param InviteAcceptsRequest $req
     * @param $token
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(InviteAcceptsRequest $req, $token)
    {
        $invite = Invite::whereToken($token)->first();

        $invite->accepted = true;
        $invite->save();

        $user = new User();
        $user->fill([
            'name'     => $req->get('name'),
            'email'    => $req->get('email'),
            'password' => bcrypt($req->get('password'))
        ])->save();

        foreach($invite->roles as $role) {
            $user->assignRole($role);
        }

        auth()->login($user);

        return redirect()->route('auctions.index')->with('success', 'Welcome to ' . config('app.name') . '!');
    }
}
