<?php

namespace App\Http\Controllers;

use App\Actions\CreateAuctionItem;
use App\Actions\UpdateAuctionItem;
use App\Http\Requests\CreateAuctionItemRequest;
use App\Http\Requests\UpdateAuctionItemRequest;
use App\Http\Resources\Auction as AuctionResource;
use App\Http\Resources\AuctionItemCollection;
use App\Http\Resources\Donation;
use App\Models\Auction;
use App\Models\AuctionItem;
use App\Models\AuctionItemType;

class AuctionItemsController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:auction.auction-items.read')->only('index');
        $this->middleware('can:auction.auction-items.create')->only('create', 'store');
        $this->middleware('can:auction.auction-items.update')->only('edit', 'update');
    }

    /**
     * @param Auction $auction
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Auction $auction)
    {
        $items = $auction->auctionItems()
            ->with('donations.vendorDonors', 'donations.memberDonors')
            ->orderBy('item_number')
            ->get();

        $json = [
            'auction'       => AuctionResource::make($auction)->jsonSerialize(),
            'auction_items' => AuctionItemCollection::make($items)->jsonSerialize()
        ];

        return view('auctions.auction-items.index')->with(compact('auction', 'json'));
    }

    /**
     * @param Auction $auction
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Auction $auction)
    {
        $types     = AuctionItemType::all();
        $donations = $auction->donations()->with('memberDonors', 'vendorDonors')
            ->orderBy('name')
            ->availableForAuction($auction)
            ->get();
        $json      = [
            'donations' => Donation::collection($donations)->jsonSerialize()
        ];

        return view('auctions.auction-items.create')->with(compact('auction', 'types', 'json'));
    }

    /**
     * @param CreateAuctionItemRequest $req
     * @param Auction $auction
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateAuctionItemRequest $req, Auction $auction)
    {
        $action = new CreateAuctionItem($req->all(), $auction);
        $res    = $action->execute();

        if ($res) {
            return redirect()->route('auctions.auction-items.index', $auction)->with('success', 'Created new auction item!');
        }

        return redirect()->back()->with('danger', 'Could not save your item. Please try again.');
    }

    /**
     * @param Auction $auction
     * @param AuctionItem $auction_item
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Auction $auction, AuctionItem $auction_item)
    {
        $types              = AuctionItemType::all();
        $donations          = $auction->donations()->with('memberDonors', 'vendorDonors')
            ->availableForAuction($auction)
            //include the donations associated to this auction item
            ->orWhereIn('donations.id', $auction_item->donations()->select('donations.id')->pluck('id')->all())
            ->get();
        $item_donations     = [];
        $item_donation_qtys = [];
        $auction_item->donations->each(function (\App\Models\Donation $donation) use (&$item_donations, &$item_donation_qtys) {
            $item_donations[]     = $donation->id;
            $item_donation_qtys[] = $donation->pivot->qty;
        });
        $json = [
            'donations'          => Donation::collection($donations)->jsonSerialize(),
            'item_donations'     => $item_donations,
            'item_donation_qtys' => $item_donation_qtys,
        ];

        return view('auctions.auction-items.edit', compact('auction', 'auction_item', 'json', 'types'));
    }

    /**
     * @param UpdateAuctionItemRequest $req
     * @param Auction $auction
     * @param AuctionItem $auction_item
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateAuctionItemRequest $req, Auction $auction, AuctionItem $auction_item)
    {
        $action = new UpdateAuctionItem($req->all(), $auction_item);
        $res    = $action->execute();

        if (!$res) {
            return redirect()->back()->with('danger', 'Could not update your item. Please try again.');
        }

        return redirect()->route('auctions.auction-items.index', $auction)->with('success', 'Updated item ' . $auction_item->name);
    }
}
