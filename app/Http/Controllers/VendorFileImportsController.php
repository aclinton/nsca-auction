<?php

namespace App\Http\Controllers;

use App\Actions\ImportVendorFile;
use App\Http\Requests\CreateVendorFileImportRequest;
use Carbon\Carbon;

class VendorFileImportsController extends Controller
{
    /**
     * @param CreateVendorFileImportRequest $req
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateVendorFileImportRequest $req)
    {
        $file_name = md5($req->user()->email . Carbon::now()->timestamp) . '.csv';
        $path      = $req->file('file')->storeAs('file-imports', $file_name, [
            'disk' => 'local'
        ]);
        $action    = new ImportVendorFile($path);
        $action->execute();

        return redirect()->back()->with('success', 'Your file is importing. Please try refreshing the page after a few seconds to see the new records.');
    }
}
