<?php

namespace App\Http\Requests;

use App\Rules\ItemDonationWithQtyNeedsQty;
use App\Rules\PreventDuplicateItemDonations;
use App\Rules\UniqueItemAuctionNumber;
use Illuminate\Foundation\Http\FormRequest;

class CreateAuctionItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'        => 'required',
            'type'        => 'required|exists:auction_item_types,id',
            'item_number' => ['required', new UniqueItemAuctionNumber],
            'description' => 'max:255',
            'donations'   => 'required',
            'donations.*' => ['exists:donations,id', new PreventDuplicateItemDonations(request()->get('donations', []))],
            'qtys'        => 'sometimes',
            'qtys.*'      => [new ItemDonationWithQtyNeedsQty(request()->get('donations', []))]
        ];
    }
}
