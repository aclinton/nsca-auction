<?php

namespace App\Http\Requests;

use App\Models\AuctionItem;
use App\Rules\ItemDonationWithQtyNeedsQty;
use App\Rules\PreventDuplicateItemDonations;
use App\Rules\UniqueItemAuctionNumber;
use Illuminate\Foundation\Http\FormRequest;

class UpdateAuctionItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /** @var AuctionItem $item */
        $item = request()->route('auction_item');

        return [
            'name'        => 'required',
            'type'        => 'required|exists:auction_item_types,id',
            'item_number' => ['required', new UniqueItemAuctionNumber($item->item_number)],
            'description' => 'max:255',
            'donations'   => 'required',
            'donations.*' => ['exists:donations,id', new PreventDuplicateItemDonations(request()->get('donations', []))],
            'qtys'        => 'sometimes',
            'qtys.*'      => [new ItemDonationWithQtyNeedsQty(request()->get('donations', []), $item->id)]
        ];
    }
}
