<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAuctionDonationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'members'       => 'required_without:vendor|exists:donors,id',
            'vendor'        => 'required_without:members|exists:vendors,id',
            'donation_type' => 'required|exists:donation_types,id',
            'name'          => 'required',
            'value'         => 'sometimes|nullable|numeric|min:0',
            'qty'           => 'sometimes|nullable|numeric|min:0'
        ];
    }
}
