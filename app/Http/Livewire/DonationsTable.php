<?php

namespace App\Http\Livewire;

use App\Models\Auction;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Livewire\Component;

class DonationsTable extends Component
{
    public $donations;
    public $auction;
    public $query;
    public $filter_donor_type;
    public $filter_donation_type;
    protected $updatesQueryString = ['query', 'filter_donor_type', 'filter_donation_type'];

    /**
     * @param Auction $auction
     */
    public function mount(Auction $auction)
    {
        $this->auction              = $auction;
        $this->query                = request('query');
        $this->filter_donor_type    = request('filter_donor_type');
        $this->filter_donation_type = request('filter_donation_type');
    }

    public function render()
    {
        $this->_filterDonations();
        return view('livewire.donations-table');
    }

    private function _filterDonations()
    {
        $q = $this->auction->donations()
            ->with(['memberDonors', 'vendorDonors.auctions' => function (BelongsToMany $q) {
                return $q->where('auctions.id', $this->auction->id);
            }, 'vendorDonors.auctions.pivot.schoolFamily'])
            ->when($this->query, function (Builder $query, $q) {
                return $query->where(function (Builder $_query) use ($q) {
                    return $_query->where('name', 'like', "%$q%")
                        ->orWhere('description', 'like', "%$q%");
                });
            })->when($this->filter_donor_type, function (Builder $query, $donor_type) {
                if ($donor_type == 'vendor') {
                    $query->hasVendorDonors()->groupBy('donations.id');
                } else if ($donor_type == 'member') {
                    $query->hasMemberDonors()->groupBy('donations.id');
                }
            })->when($this->filter_donation_type, function (Builder $query, $donation_type) {
                $query->forType($donation_type);
            });

        $this->donations = $q->get();
    }
}
