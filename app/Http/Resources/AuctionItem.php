<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AuctionItem extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'name'        => $this->name,
            'item_number' => $this->item_number,
            'description' => $this->description,
            'accepted'    => $this->accepted,
            'qty'         => $this->qty,
            'value'       => $this->value,
            'auction'     => Auction::make($this->whenLoaded('auction')),
            'donations'   => DonationCollection::make($this->whenLoaded('donations')),
            'type'        => AuctionItemType::make($this->whenLoaded('type')),
            'edit_url'    => route('auctions.auction-items.edit', ['auction' => $this->resource->auction, 'auction_item' => $this->resource]),
        ];
    }
}
