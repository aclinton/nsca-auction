<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Donation extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $auction_present = !is_null(request()->route('auction'));

        $data = [
            'id'            => $this->id,
            'auction'       => Auction::make($this->whenLoaded('auction')),
            'accepted'      => $this->accepted,
            'description'   => $this->description,
            'name'          => $this->name,
            'qty'           => $this->qty,
            'type'          => DonationType::make($this->whenLoaded('type')),
            'value'         => $this->value,
            'vendor_donors' => Vendor::collection($this->whenLoaded('vendorDonors')),
            'member_donors' => DonorCollection::make($this->whenLoaded('memberDonors')),
            'meta'          => $this->whenPivotLoaded('auction_item_donation', function () {
                return [
                    'qty' => $this->pivot->qty
                ];
            })
        ];

        $data['qty_available'] = $this->qty;

        if ($auction_present && !is_null($this->qty)) {
            $data['qty_available'] = $this->resource->getAvailableQtyForAuction(request()->route('auction'));
        }

        return $data;
    }
}
