<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Vendor extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id'        => $this->id,
            'name'      => $this->name,
            'url'       => $this->url,
            'email'     => $this->email,
            'phone'     => $this->phone,
            'contact'   => $this->contact_name,
            'location'  => [
                'address' => $this->address,
                'city'    => $this->city,
                'state'   => $this->state,
                'zip'     => $this->zip,
            ],
            'status' => $this->whenPivotLoaded('auction_vendor', function () {
                if(!$this->pivot->status) {
                    return null;
                }

                return VendorStatus::make($this->pivot->status)->jsonSerialize();
            }),
            'school_family' => $this->whenPivotLoaded('auction_vendor', function () {
                if(!$this->pivot->schoolFamily) {
                    return null;
                }

                return SchoolFamily::make($this->pivot->schoolFamily)->jsonSerialize();
            }),
            'edit_url' => route('vendors.edit', $this->resource)
        ];

        return $data;
    }
}
