<?php


namespace App\Actions;


use App\FileImporters\VendorImporter;
use App\Models\FileImport;
use App\Models\Vendor;

class ImportVendorFile
{
    /**
     * @var string
     */
    private $file_path;

    /**
     * ImportVendorFile constructor.
     * @param string $file_path
     */
    public function __construct(string $file_path)
    {
        $this->file_path = $file_path;
    }

    public function execute()
    {
        $importer               = new VendorImporter();
        $file_import            = new FileImport();
        $file_import->type      = (new Vendor())->getMorphClass();
        $file_import->file_path = $this->file_path;
        $file_import->save();

        $importer->import($file_import);
    }
}