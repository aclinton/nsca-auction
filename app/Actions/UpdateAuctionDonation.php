<?php


namespace App\Actions;


use App\Models\Donation;
use Illuminate\Support\Arr;

class UpdateAuctionDonation
{
    /**
     * @var array
     */
    private $data;

    /**
     * @var Donation
     */
    private $donation;

    /**
     * UpdateAuctionDonation constructor.
     * @param array $data
     * @param Donation $donation
     */
    public function __construct(array $data, Donation $donation)
    {
        $this->data = $data;
        $this->donation = $donation;
    }

    public function execute()
    {
        $this->donation->donation_type_id = $this->data['donation_type'];
        $this->donation->fill(Arr::only($this->data, ['name', 'description', 'qty', 'value']));
        $this->donation->save();

        if ($this->data['donation_donor_type'] == 'vendor') {
            $this->donation->vendorDonors()->sync($this->data['vendor']);
            $this->donation->memberDonors()->sync([]);
        } else {
            $this->donation->memberDonors()->sync($this->data['members']);
            $this->donation->vendorDonors()->sync([]);
        }
    }
}
