<?php


namespace App\Actions;


use App\Models\Invite;

class SendNewEmailInvite
{
    /**
     * @var string
     */
    private $email;

    /**
     * @var null
     */
    private $roles;

    /**
     * SendEmailInvitation constructor.
     * @param string $email
     * @param array $roles
     */
    public function __construct(string $email, ?array $roles = [])
    {
        $this->email = $email;
        $this->roles = $roles;
    }

    public function execute()
    {
        $token         = Invite::getToken();
        $invite        = new Invite();
        $invite->token = $token;
        $invite->email = $this->email;
        $invite->save();

        if ($this->roles && count($this->roles)) {
            $invite->roles()->sync($this->roles);
        }

        $action = new SendEmailInvite($invite);
        $action->execute();
    }
}
