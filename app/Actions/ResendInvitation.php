<?php


namespace App\Actions;


use App\Models\Invite;

class ResendInvitation
{
    /**
     * @var Invite
     */
    private $invite;

    /**
     * ResendInvitation constructor.
     * @param Invite $invite
     */
    public function __construct(Invite $invite)
    {
        $this->invite = $invite;
    }

    public function execute()
    {
        $this->invite->update(['accepted' => false]);

        $action = new SendEmailInvite($this->invite);
        $action->execute();
    }
}
