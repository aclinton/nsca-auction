<?php


namespace App\Actions;


use App\Models\Auction;
use App\Models\AuctionItem;
use Illuminate\Support\Arr;

class CreateAuctionItem
{
    /**
     * @var array
     */
    private $data;

    /**
     * @var Auction
     */
    private $auction;

    /**
     * CreateAuctionItem constructor.
     * @param array $data
     * @param Auction $auction
     */
    public function __construct(array $data, Auction $auction)
    {
        $this->data    = $data;
        $this->auction = $auction;
    }

    /**
     * @return bool
     */
    public function execute()
    {
        /** @var AuctionItem $item */
        $item = AuctionItem::create([
            'name'                 => Arr::get($this->data, 'name'),
            'description'          => Arr::get($this->data, 'description'),
            'auction_item_type_id' => Arr::get($this->data, 'type'),
            'item_number'          => Arr::get($this->data, 'item_number')
        ]);
        $item->auction()->associate($this->auction)->save();
        $pivot_data = [];

        collect(Arr::get($this->data, 'donations'))->each(function ($id, $index) use (&$pivot_data) {
            $qty = $this->data['qtys'][$index] ?? null;

            $pivot_data[$id] = ['qty' => $qty];
        });

        $item->donations()->sync($pivot_data);

        return true;
    }
}
