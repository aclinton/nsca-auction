<?php


namespace App\Actions;


use App\Models\Auction;
use App\Models\Donation;
use Illuminate\Support\Arr;

class CreateAuctionDonation
{
    /**
     * @var array
     */
    private $data;

    /**
     * @var Auction
     */
    private $auction;

    /**
     * CreateAuctionDonation constructor.
     * @param array $data
     * @param Auction $auction
     */
    public function __construct(array $data, Auction $auction)
    {
        $this->data    = $data;
        $this->auction = $auction;
    }

    public function execute()
    {
        $donation = new Donation();
        $donation->donation_type_id = $this->data['donation_type'];
        $donation->fill(Arr::only($this->data, ['name', 'description', 'qty', 'value']));
        $donation->auction()->associate($this->auction);
        $donation->save();

        if ($this->data['donation_donor_type'] == 'vendor') {
            $donation->vendorDonors()->sync($this->data['vendor']);
        } else {
            $donation->memberDonors()->sync($this->data['members']);
        }
    }
}
