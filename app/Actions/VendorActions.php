<?php


namespace App\Actions;


use App\Models\Vendor;
use Illuminate\Foundation\Http\FormRequest;

class VendorActions
{
    /**
     * @param FormRequest $request
     * @return Vendor|\Illuminate\Database\Eloquent\Model
     */
    public function create(FormRequest $request)
    {
        return Vendor::create($request->all());
    }
}
