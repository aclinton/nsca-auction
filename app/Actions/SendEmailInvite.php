<?php


namespace App\Actions;


use App\Jobs\SendInvite;
use App\Models\Invite;
use Mail;

class SendEmailInvite
{
    /**
     * @var Invite
     */
    private $invite;

    /**
     * SendEmailInvite constructor.
     * @param Invite $invite
     */
    public function __construct(Invite $invite)
    {
        $this->invite = $invite;
    }

    public function execute()
    {
        SendInvite::dispatch($this->invite)->onQueue('emails');
    }
}
