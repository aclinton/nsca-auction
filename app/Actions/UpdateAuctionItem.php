<?php


namespace App\Actions;


use App\Models\AuctionItem;
use Illuminate\Support\Arr;

class UpdateAuctionItem
{
    /**
     * @var array
     */
    private $data;

    /**
     * @var AuctionItem
     */
    private $item;

    /**
     * UpdateAuctionItem constructor.
     * @param array $data
     * @param AuctionItem $item
     */
    public function __construct(array $data, AuctionItem $item)
    {
        $this->data = $data;
        $this->item = $item;
    }

    public function execute()
    {
        /** @var AuctionItem $item */
        $this->item->fill([
            'name'                 => Arr::get($this->data, 'name'),
            'description'          => Arr::get($this->data, 'description'),
            'auction_item_type_id' => Arr::get($this->data, 'type'),
            'item_number'          => Arr::get($this->data, 'item_number')
        ])->save();
        $pivot_data = [];

        collect(Arr::get($this->data, 'donations'))->each(function ($id, $index) use (&$pivot_data) {
            $qty = $this->data['qtys'][$index] ?? null;

            $pivot_data[$id] = ['qty' => $qty];
        });

        $this->item->donations()->sync($pivot_data);

        return true;
    }
}
