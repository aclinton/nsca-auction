<?php


namespace App\Actions;


use App\Models\Auction;

class RegisterAuctionVendors
{
    /**
     * @var Auction
     */
    private $auction;

    /**
     * @var array
     */
    private $vendor_ids;

    /**
     * RegisterAuctionVendors constructor.
     * @param Auction $auction
     * @param array $vendor_ids
     */
    public function __construct(Auction $auction, array $vendor_ids)
    {
        $this->auction = $auction;
        $this->vendor_ids = $vendor_ids;
    }

    public function execute()
    {
        //todo: fix hardcoded status ID
        $pivot_data = collect($this->vendor_ids)->mapWithKeys(function ($id) {
            return [$id => ['vendor_status_id' => 1]];
        })->all();
        $this->auction->vendors()->attach($pivot_data);
    }
}