<?php

namespace App\Console\Commands;

use App\Mail\DonationReport;
use App\Models\Auction;
use App\Models\Donation;
use App\Models\Donor;
use App\Models\Vendor;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use League\Csv\Writer;
use Mail;

class GenerateDonationsReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:donations
    {auction : Specify the auction donations to report}
    {--email= : Send report to a comma separated list of emails}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a donation report';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $auction = $this->argument('auction');
        $auction = Auction::findOrFail($auction);
        $emails = $this->option('email');
        $path    = storage_path('app/reports/donations');

        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0766, true, true);
        }

        $final_path = storage_path('app/reports/donations/' . $auction->year . "-donations-report-" . now()->timestamp . ".csv");
        $csv        = Writer::createFromPath($final_path, 'w');

        $csv->insertOne([
            'Name',
            'Description',
            'Donation Type',
            'Donors',
            'Qty',
            'Value'
        ]);

        Donation::with('memberDonors', 'vendorDonors')
            ->forAuction($auction)
            ->orderBy('name')
            ->get()
            ->each(function (Donation $donation) use ($csv) {
                $donors = 'No donors';

                if ($donation->memberDonors->count()) {
                    $donors = $donation->memberDonors->map(function (Donor $donor) {
                        return $donor->name;
                    })->implode(', ');
                } else if ($donation->vendorDonors->count()) {
                    $donors = $donation->vendorDonors->map(function (Vendor $vendor) {
                        return $vendor->name;
                    })->implode(', ');
                }

                $csv->insertOne([
                    $donation->name,
                    $donation->description,
                    $donation->type->name,
                    $donors,
                    $donation->qty,
                    $donation->value
                ]);
            });

        $this->info("Report location: $final_path");

        if(!empty($emails)) {
            $emails = array_map('trim', explode(',', $emails));
            $email = new DonationReport($auction, $final_path);

            Mail::to($emails)->send($email);
        }

        return 1;
    }
}
