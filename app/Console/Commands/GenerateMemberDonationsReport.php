<?php

namespace App\Console\Commands;

use App\Models\Auction;
use App\Models\Donation;
use App\Models\Donor;
use App\Models\Vendor;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\File;
use League\Csv\Writer;

class GenerateMemberDonationsReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:donations:member
    {auction : Specify the auction donations to report}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a member donation report';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $auction = $this->argument('auction');
        $auction = Auction::findOrFail($auction);
        $path    = storage_path('app/reports/member-donations');

        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0766, true, true);
        }
        $csv = Writer::createFromPath(
            storage_path('app/reports/donations/' . $auction->year . "-member-donations-report-" . now()->timestamp . ".csv"),
            'w'
        );
        $csv->insertOne([
            'Name',
            'Description',
            'Donors',
            'Qty',
            'Value'
        ]);

        Donation::with('memberDonors', 'vendorDonors')->whereHas('auction', function (Builder $q) use ($auction) {
            $q->where('auction_id', $auction->id);
        })
            ->orderBy('name')
            ->get()
            ->filter(function (Donation $donation) {
                return $donation->vendorDonors->count() < 1 && $donation->memberDonors->count() > 0;
            })->each(function (Donation $donation) use ($csv) {
                $csv->insertOne([
                    $donation->name,
                    $donation->description,
                    $donation->memberDonors->map(function (Donor $donor) {
                        return $donor->name;
                    })->implode(', '),
                    $donation->qty,
                    $donation->value
                ]);
            });

        return 1;
    }
}
