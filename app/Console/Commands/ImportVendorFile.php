<?php

namespace App\Console\Commands;

use App\FileImporters\VendorImporter;
use App\Models\FileImport;
use App\Models\Vendor;
use Illuminate\Console\Command;

class ImportVendorFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:vendors
    {--path= : CSV path}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import a vendor CSV file';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = $this->option('path');

        if (!is_file($path)) {
            $this->error("$path is not a valid file");
            return 0;
        }

        $importer               = new VendorImporter();
        $file_import            = new FileImport();
        $file_import->type      = (new Vendor())->getMorphClass();
        $file_import->file_path = $path;
        $file_import->save();

        $importer->import($file_import);

        return 1;
    }
}
