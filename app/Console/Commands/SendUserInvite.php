<?php

namespace App\Console\Commands;

use App\Actions\SendEmailInvite;
use App\Actions\SendNewEmailInvite;
use App\Models\Invite;
use App\Models\User;
use Illuminate\Console\Command;
use Spatie\Permission\Models\Role;

class SendUserInvite extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invite:send
    {email : Specify the email to send to}
    {--role= : Specify the role name to attach to the invite}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a registration invite to a given email.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email     = $this->argument('email');
        $role_name = $this->option('role');

        if (empty($email)) {
            $this->error("Please enter an email");
            return 0;
        }

        if (Invite::whereEmail($email)->exists()) {
            $invite = Invite::whereEmail($email)->first();

            if (!$invite->accepted) {
                $confirm = $this->confirm("Invite already exists. Resend?");

                if ($confirm) {
                    $action = new SendEmailInvite($invite);
                    $action->execute();

                    $this->info("Invite sent to => $email");

                    return 1;
                } else {
                    return 0;
                }
            } else {
                $this->error("An invite for this email already exists");
                return 0;
            }
        }

        if (User::whereEmail($email)->exists()) {
            $this->error("An user with this email already exists");
            return 0;
        }

        if($role_name) {
            $role = Role::whereName($role_name)->firstOrFail();
        }

        $action = new SendNewEmailInvite($email, $role->id ?? null);
        $action->execute();

        $this->info("Invite sent to => $email");

        return 1;
    }
}
