<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Donor extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return mixed
     */
    public function getFirstNameAttribute()
    {
        $parts = explode(' ', $this->attributes['name'], 2);

        return collect($parts)->filter(function($p) {
            return !empty($p);
        })->first();
    }

    /**
     * @return mixed
     */
    public function getLastNameAttribute()
    {
        $parts = explode(' ', $this->attributes['name'], 2);

        return collect($parts)->filter(function($p) {
            return !empty($p);
        })->last();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function donations()
    {
        return $this->morphMany(Donation::class, 'donor');
    }
}
