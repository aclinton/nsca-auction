<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class Invite extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $casts = [
        'accepted' => 'boolean'
    ];

    /**
     * @var array
     */
    protected $dates = ['invite_sent_at'];

    /**
     * @var array
     */
    protected $fillable = [
        'email', 'token', 'invite_sent_at'
    ];

    /**
     * @return string
     */
    public static function getToken()
    {
        $token = Str::random();

        while(static::where('token', $token)->exists()) {
            $token = Str::random();
        }

        return $token;
    }

    /**
     * @return string
     */
    public function getAcceptUrlAttribute()
    {
        return route('invite.view', $this->token);
    }

    /**
     * @return bool
     */
    public function getSentAttribute()
    {
        return !is_null($this->invite_sent_at);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class)->withTimestamps();
    }
}
