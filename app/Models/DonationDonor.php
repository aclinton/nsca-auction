<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Relations\MorphPivot;

class DonationDonor extends MorphPivot
{
    /**
     * @var bool
     */
    public $incrementing = true;
}