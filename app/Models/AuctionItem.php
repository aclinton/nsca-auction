<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AuctionItem extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    protected $with = ['type'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function auction()
    {
        return $this->belongsTo(Auction::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function donations()
    {
        return $this->belongsToMany(Donation::class)->withTimestamps()->withPivot(['qty']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(AuctionItemType::class, 'auction_item_type_id');
    }

    /**
     * @param Builder $q
     * @return Builder
     */
    public function scopeLive(Builder $q)
    {
        return $q->whereHas('type', function (Builder $_q) {
            $_q->where('name', 'live');
        });
    }

    /**
     * @param Builder $q
     * @return Builder
     */
    public function scopeSilent(Builder $q)
    {
        return $q->whereHas('type', function (Builder $_q) {
            $_q->where('name', 'silent');
        });
    }

    /**
     * @param Builder $q
     * @param Auction $auction
     * @return Builder
     */
    public function scopeForAuction(Builder $q, Auction $auction)
    {
        return $q->whereHas('auction', function (Builder $_q) use($auction) {
            $_q->where('auctions.id', $auction->id);
        });
    }
}
