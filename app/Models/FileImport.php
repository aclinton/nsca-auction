<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Storage;

class FileImport extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $dates = ['imported_at', 'failed_at'];

    /**
     * @var array
     */
    protected $fillable = ['records', 'file_path', 'imported_at', 'failed_at', 'type'];

    /**
     * @return string
     */
    public function getFullFilePathAttribute()
    {
        $path = Storage::disk('local')->path($this->file_path);

        return $path;
    }
}
