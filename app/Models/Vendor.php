<?php

namespace App\Models;

use AjCastro\EagerLoadPivotRelations\EagerLoadPivotTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vendor extends Model
{
    use SoftDeletes, EagerLoadPivotTrait;

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'url', 'email', 'phone', 'contact_name', 'address', 'city', 'zip', 'state', 'logo',
        'discontinued', 'notes'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'discontinued' => 'boolean'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function auctions()
    {
        return $this->belongsToMany(Auction::class)
            ->withTimestamps()
            ->withPivot('vendor_status_id')
            ->withPivot('school_family_id')
            ->using(AuctionVendor::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function donations()
    {
        return $this->morphMany(Donation::class, 'donor');
    }

    /**
     * @param Builder $q
     * @param Auction $auction
     * @return Builder
     */
    public function scopeNotRegisteredToAuction(Builder $q, Auction $auction)
    {
        return $q->whereDoesntHave('auctions', function (Builder $_q) use($auction) {
            return $_q->where('auctions.id', $auction->id);
        });
    }
}
