<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SchoolFamily extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['family_last_name', 'first_parent_name', 'second_parent_name'];

    /**
     * @var array
     */
    protected $appends = ['display_name'];

    /**
     * @return string
     */
    public function getDisplayNameAttribute()
    {
        $last_segment = [];

        if ($this->first_parent_name) {
            $last_segment[] = $this->first_parent_name;
        }

        if ($this->second_parent_name) {
            $last_segment[] = $this->second_parent_name;
        }

        $last_segment = implode(' & ', $last_segment);
        $parts        = [$this->family_last_name, $last_segment];

        return implode(', ', $parts);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function auctionVendors()
    {
        return $this->hasMany(AuctionVendor::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function auctions()
    {
        return $this->belongsToMany(Auction::class)->withTimestamps();
    }
}
