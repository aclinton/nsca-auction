<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Donation extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    protected $with = ['type'];

    /**
     * @var array
     */
    protected $casts = [
        'accepted' => 'boolean'
    ];

    /**
     * @return bool
     */
    public function getRejectedAttribute()
    {
        return !$this->accepted;
    }

    /**
     * @return bool
     */
    public function getHasVendorDonorsAttribute()
    {
        return $this->vendorDonors()->count() > 0;
    }

    /**
     * @return bool
     */
    public function getHasMemberDonorsAttribute()
    {
        return $this->memberDonors()->count() > 0;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function auction()
    {
        return $this->belongsTo(Auction::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function auctionItems()
    {
        return $this->belongsToMany(AuctionItem::class)
            ->withTimestamps()
            ->withPivot(['qty']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function memberDonors()
    {
        return $this->morphedByMany(Donor::class, 'donor', 'donation_donor')
            ->using(DonationDonor::class)
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function vendorDonors()
    {
        return $this->morphedByMany(Vendor::class, 'donor', 'donation_donor')
            ->using(DonationDonor::class)
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(DonationType::class, 'donation_type_id');
    }

    /**
     * @param Builder $q
     * @param Auction $auction
     * @return Builder
     */
    public function scopeForAuction(Builder $q, Auction $auction)
    {
        return $q->whereHas('auction', function (Builder $q) use($auction) {
            $q->where('auctions.id', $auction->id);
        });
    }

    /**
     * @param Builder $q
     * @param string $type
     * @return Builder
     */
    public function scopeForType(Builder $q, string $type)
    {
        return $q->whereHas('type', function (Builder $_q) use ($type) {
            $_q->forType($type);
        });
    }

    /**
     * @param Builder $q
     * @param Auction $auction
     * @return Builder
     */
    public function scopeAvailableForAuction(Builder $q, Auction $auction)
    {
        $sub_q = Donation::query()
            ->join('auction_item_donation as aid', 'aid.donation_id', '=', 'donations.id')
            ->where('donations.auction_id', $auction->id)
            ->groupBy('aid.donation_id')
            ->selectRaw("donations.id as d_id, coalesce(sum(aid.qty), 0) as used");

        return $q->leftJoinSub($sub_q, 'totals', function ($join) {
            $join->on('totals.d_id', '=', 'donations.id');
        })
            ->where(function (Builder $q) {
                $q->where(function (Builder $q) {
                    $q->whereNotNull('totals.used')
                        ->whereNotNull('donations.qty')
                        ->whereRaw("donations.qty - totals.used > 0");
                })
                    ->orWhere(function (Builder $q) {
                        $q->whereNotNull('donations.qty')
                            ->whereNull('totals.used');
                    })->orWhere(function (Builder $q) {
                        $q->whereNull('donations.qty')->whereDoesntHave('auctionItems');
                    });
            })
            ->select('donations.*'); //necessary to prevent column overwriting from join
    }

    /**
     * @param Builder $q
     */
    public function scopeHasMemberDonors(Builder $q)
    {
        $q->rightJoin('donation_donor as member_dd', function ($join) {
            $join->on('member_dd.donation_id', '=', 'donations.id')
                ->where('member_dd.donor_type', '=', (new Donor)->getMorphClass());
        })
            ->select('donations.*');
    }

    /**
     * @param Builder $q
     */
    public function scopeHasVendorDonors(Builder $q)
    {
        $q->rightJoin('donation_donor as vendor_dd', function ($join) {
            $join->on('vendor_dd.donation_id', '=', 'donations.id')
                ->where('vendor_dd.donor_type', '=', (new Vendor)->getMorphClass());
        })
            ->select('donations.*');
    }

    /* Move these to some other class */

    /**
     * @param Auction $auction
     * @param array $ignore_ids
     * @return int|mixed|null
     */
    public function getAvailableQtyForAuction(Auction $auction, array $ignore_ids = [])
    {
        $pivots   = $this->auctionItems()->forAuction($auction)->get();
        $qty_used = $pivots->map(function (AuctionItem $item) use ($ignore_ids) {
            return in_array($item->id, $ignore_ids) ? 0 : $item->pivot->qty;
        })->sum();

        return ($this->qty ?? 0) - $qty_used;
    }
}
