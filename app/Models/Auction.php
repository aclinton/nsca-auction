<?php

namespace App\Models;

use AjCastro\EagerLoadPivotRelations\EagerLoadPivotTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Str;

class Auction extends Model
{
    use SoftDeletes, EagerLoadPivotTrait;

    /**
     * @var array
     */
    protected $fillable = ['year', 'code', 'event_date'];

    /**
     * @var array
     */
    protected $dates = ['event_date'];

    /**
     * @return Donor[]|\Illuminate\Database\Eloquent\Collection|Builder[]|\Illuminate\Support\Collection
     */
    public function getMemberDonorsAttribute()
    {
        $member_donations = $this->donations()->whereHas('memberDonors')->with('memberDonors')->get();
        $ids              = $member_donations->pluck('memberDonors.*.id')->unique();

        return Donor::whereIn('id', $ids)->get();
    }

    /**
     * @return Vendor[]|\Illuminate\Database\Eloquent\Collection|Builder[]|\Illuminate\Support\Collection
     */
    public function getVendorDonorsAttribute()
    {
        $vendor_donations = $this->donations()->whereHas('vendorDonors')->with('vendorDonors')->get();
        $ids              = $vendor_donations->pluck('vendorDonors.*.id')->unique();

        return Vendor::whereIn('id', $ids)->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function auctionItems()
    {
        return $this->hasMany(AuctionItem::class);
    }

    /**
     * @return mixed
     */
    public function silentAuctionItems()
    {
        return $this->auctionItems()->silent();
    }

    /**
     * @return mixed
     */
    public function liveAuctionItems()
    {
        return $this->auctionItems()->live();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function donations()
    {
        return $this->hasMany(Donation::class)->orderBy('name');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function participatingVendors()
    {
        //todo: query specific vendor status id
        return $this->vendors()->wherePivot('vendor_status_id', 4);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function schoolFamilies()
    {
        return $this->belongsToMany(SchoolFamily::class)->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function vendors()
    {
        return $this->belongsToMany(Vendor::class)
            ->withTimestamps()
            ->withPivot('vendor_status_id')
            ->withPivot('school_family_id')
            ->orderBy('name')
            ->using(AuctionVendor::class);
    }

    /**
     * @return mixed
     */
    public static function getNewCode()
    {
        $code = Str::random(8);

        while(Auction::whereCode($code)->exists()) {
            $code = Str::random(8);
        }

        return $code;
    }
}
