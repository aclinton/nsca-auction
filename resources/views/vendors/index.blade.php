@extends('layouts.app')

@section('title', 'Vendors | ')

@section('content')
    <div class="row mt-2">
        <div class="col">
            <div class="card nsca-card nsca-card__table">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        <h1 class="text-size-1_5 mb-0">Vendors</h1>
                        <div class="d-flex flex-row">
                            @can('vendors.create')
                            <a role="button" href="{{route('vendors.create')}}" class="btn btn-primary mr-2">
                                Add Vendor
                            </a>
                            @endcan
                            @role('super-admin')
                            <modal-trigger target="#vendor-file-import-modal">
                                <template #default="slotProps">
                                    <button class="btn btn-primary" @click="slotProps.triggerModal">
                                        <i class="fa fa-upload" aria-hidden="true"></i></button>
                                </template>
                            </modal-trigger>
                            @endrole
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    @if($vendors->count() < 1)
                        <div class="alert alert-primary" role="alert">
                            No vendors have been imported yet.
                        </div>
                    @else
                        <table class="table table-hover table-responsive-lg table-nsca">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Address</th>
                                <th>Contact</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Discontinued</th>
                                <th>Notes</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($vendors as $vendor)
                                <tr>
                                    <td>
                                        @if($vendor->url)
                                            <a target="_blank" href="{{$vendor->url}}">
                                                {{$vendor->name}}
                                            </a>
                                        @else
                                            {{$vendor->name}}
                                        @endif
                                    </td>
                                    <td>{{$vendor->address}}</td>
                                    <td>{{$vendor->contact_name}}</td>
                                    <td>{{$vendor->email}}</td>
                                    <td>{{$vendor->phone}}</td>
                                    <td>{{$vendor->discontinued ? 'yes' : 'no'}}</td>
                                    <td>
                                        @if($vendor->notes)
                                            <button class="btn btn-primary">View</button>
                                        @else
                                            <i>No notes</i>
                                        @endif
                                    </td>
                                    <td class="text-right">
                                        <a href="{{route('vendors.edit', $vendor)}}" class="btn btn-primary">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <vendor-file-import-modal id="vendor-file-import-modal"></vendor-file-import-modal>
@endsection
