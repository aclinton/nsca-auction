@extends('layouts.app')

@section('title', 'Create Vendor | ')

@section('breadcrumbs')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="{{route('vendors.index')}}">Vendors</a></li>
            <li class="breadcrumb-item active" aria-current="page">Create Vendor</li>
        </ol>
    </nav>
@endsection

@section('content')
<div class="card nsca-card">
    <div class="card-header">
        <h3 class="card-title mb-0">New Vendor</h3>
    </div>
    <div class="card-body">
        <form action="{{route('vendors.store')}}" method="POST">
            @csrf
            <div class="form-group row">
                <label class="col-form-label col-sm-3 col-md-4 text-md-right" for="name">Name</label>
                <div class="col-sm-9 col-md-8">
                    <input
                        class="form-control @if($errors->has('name'))is-invalid @endif"
                        value="{{old('name')}}"
                        type="text"
                        name="name"
                        id="name">
                    @if($errors->has('name'))
                        <div class="invalid-feedback">
                            @foreach($errors->get('name') as $error)
                                <div>
                                    {{$error}}
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-sm-3 col-md-4 text-md-right" for="url">URL</label>
                <div class="col-sm-9 col-md-8">
                    <input class="form-control" value="{{old('url')}}" type="text" name="url" id="url">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-9 col-md-8 offset-sm-3 offset-md-4">
                    <div>
                        <input type="hidden" name="discontinued" value="0">
                        <label class="col-form-label" for="discontinued">
                            <input type="checkbox" name="discontinued" id="discontinued" value="1">
                            Discontinued
                        </label>
                    </div>
                    <div class="mt-1">
                        <i>If {{config('app.name')}} no longer uses this vendor, it should be discontinued.</i>
                    </div>
                </div>
            </div>
            <hr>
            <h4>Contact Info</h4>
            <div class="form-group row">
                <label class="col-form-label col-sm-3 col-md-4 text-md-right" for="email">Email</label>
                <div class="col-sm-9 col-md-8">
                    <input class="form-control @if($errors->has('email'))is-invalid @endif"
                           value="{{old('email')}}"
                           type="email"
                           name="email"
                           id="email">
                    @if($errors->has('email'))
                        <div class="invalid-feedback">
                            @foreach($errors->get('email') as $error)
                                <div>
                                    {{$error}}
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-sm-3 col-md-4 text-md-right" for="phone">Phone</label>
                <div class="col-sm-9 col-md-8">
                    <input class="form-control" value="{{old('phone')}}" type="tel" name="phone" id="phone">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-sm-3 col-md-4 text-md-right" for="contact_name">Contact Name</label>
                <div class="col-sm-9 col-md-8">
                    <input class="form-control" value="{{old('contact_name')}}" type="text" name="contact_name" id="contact_name">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-sm-3 col-md-4 text-md-right" for="address">Address</label>
                <div class="col-sm-9 col-md-8">
                    <input class="form-control" value="{{old('address')}}" type="text" name="address" id="address">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-sm-3 col-md-4 text-md-right" for="city">City</label>
                <div class="col-sm-3 col-md-3">
                    <input class="form-control" value="{{old('city')}}" type="text" name="city" id="city">
                </div>
                <label class="col-form-label col-sm-2 col-md-2" for="state">State</label>
                <div class="col-sm-4 col-md-3">
                    <select class="form-control" name="state" id="state">
                        <option value="">-- Select State --</option>
                        @foreach(config('states') as $key => $value)
                            <option value="{{$key}}" @if(old('state') && old('state') == $key)selected @endif>{{$value}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-sm-3 col-md-4 text-md-right" for="zip">Zip</label>
                <div class="col-sm-9 col-md-8">
                    <input class="form-control" value="{{old('zip')}}" type="text" name="zip" id="zip">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-sm-3 col-md-4 text-md-right" for="notes">Notes</label>
                <div class="col-sm-9 col-md-8">
                    <textarea class="form-control" name="notes" id="notes" rows="5">{{old('notes')}}</textarea>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-9 col-md-8 offset-sm-3 offset-md-4">
                    <button type="submit" class="btn btn-primary">Create</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
