@extends('layouts.app')

@section('content')
    {{-- Goes above left/right content --}}
    @yield('headlineContent')

    <div class="row mt-3">
        <div class="d-sm-none d-md-block col-md-3">
            <div class="side-nav">
                <div class="side-nav__item">
                    <a href="{{route('auctions.school-families.index', $auction)}}"
                       class="d-flex align-items-center @if(Route::is('auctions.school-families.*'))active @endif">
                        <span class="side-nav__item-text"><i
                                class="fa mr-2 fa-address-book-o"></i>School Families</span>
                    </a>
                </div>

                <div class="side-nav__item">
                    <a href="{{route('auctions.vendors.index', $auction)}}"
                       class="d-flex align-items-center @if(Route::is('auctions.vendors.*'))active @endif">
                        <span class="side-nav__item-text"><i class="fa mr-2 fa-building-o"></i>Vendors</span>
                    </a>
                </div>

                <div class="side-nav__item">
                    <a href="{{route('auctions.donations.index', $auction)}}"
                       class="d-flex align-items-center @if(Route::is('auctions.donations.*'))active @endif">
                        <span class="side-nav__item-text"><i class="fa mr-2 fa-archive"></i>Donations</span>
                    </a>
                </div>

                <div class="side-nav__item">
                    <a href="{{route('auctions.auction-items.index', $auction)}}"
                       class="d-flex align-items-center @if(Route::is('auctions.auction-items.*'))active @endif">
                        <span class="side-nav__item-text"><i class="fa mr-2 fa-shopping-basket"></i>Auction Items</span>
                    </a>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-9">
            @yield('rightContent')
        </div>
    </div>
@endsection
