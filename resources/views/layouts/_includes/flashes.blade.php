@php
    $types = ['primary', 'secondary', 'success', 'danger', 'warning', 'info', 'light', 'dark'];
    $has_flash_type = false;

    foreach($types as $type) {
        if(session()->has($type)) {
            $has_flash_type = true;
            break;
        }
    }
@endphp

@if($has_flash_type)
    <div class="row justify-content-center">
        <div class="col col-md-6">
            @foreach(['primary', 'secondary', 'success', 'danger', 'warning', 'info', 'light', 'dark'] as $type)
                @if(session($type))
                    <div class="alert alert-{{$type}} fade show alert-dismissible" role="alert">
                        <div>{{session($type)}}</div>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
@endif
