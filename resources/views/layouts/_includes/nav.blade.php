<nav class="navbar navbar-expand-md navbar-dark bg-primary shadow-sm">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
        @auth
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
                @if(Request::routeIs('auctions.school-families.*') || Request::routeIs('auctions.vendors.*') || Request::routeIs('auctions.donations.*') || Request::routeIs('auctions.auction-items.*'))
                    <li class="nav-item d-none d-md-block @if(Route::is('auctions.index'))active @endif">
                        <a href="{{route('auctions.index')}}" class="nav-link">Auctions</a>
                    </li>
                    <li class="nav-item dropdown d-md-none @if(Route::is('auctions.*'))active @endif">
                        <a class="nav-link dropdown-toggle" href="#" id="auction-dropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Auctions
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item @if(Request::routeIs('auctions.school-families.*'))active @endif"
                               href="{{route('auctions.school-families.index', $auction)}}">School Families</a>
                            <a class="dropdown-item @if(Request::routeIs('auctions.vendors.*'))active @endif"
                               href="{{route('auctions.vendors.index', $auction)}}">Vendors</a>
                            <a class="dropdown-item @if(Request::routeIs('auctions.donations.*'))active @endif"
                               href="{{route('auctions.donations.index', $auction)}}">Donations</a>
                            <a class="dropdown-item @if(Request::routeIs('auctions.auction-items.*'))active @endif"
                               href="{{route('auctions.auction-items.index', $auction)}}">Auction Items</a>
                        </div>
                    </li>
                @else
                    <li class="nav-item @if(Request::routeIs('auctions.*'))active @endif">
                        <a href="{{route('auctions.index')}}" class="nav-link">Auctions</a>
                    </li>
                @endif

                {{--
                @if(Route::is('auctions.*') and !Route::is('auctions.index'))
                    <li class="nav-item @if(Route::is('auctions.school-families.*'))active @endif">
                        <a href="{{route('auctions.school-families.index', $auction)}}" class="nav-link">School Families</a>
                    </li>
                    <li class="nav-item @if(Route::is('auctions.vendors.*'))active @endif">
                        <a href="{{route('auctions.vendors.index', $auction)}}" class="nav-link">Vendors</a>
                    </li>
                    <li class="nav-item @if(Route::is('auctions.donations.*'))active @endif">
                        <a href="{{route('auctions.donations.index', $auction)}}" class="nav-link">Donations</a>
                    </li>
                    <li class="nav-item @if(Route::is('auctions.auction-items.*'))active @endif">
                        <a href="{{route('auctions.auction-items.index', $auction)}}" class="nav-link">Auction Items</a>
                    </li>
                @endif
                --}}
            </ul>
        @endauth

        <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            @impersonating <i class="fa fa-user-secret"></i> @endImpersonating{{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            @impersonating
                            <a href="{{route('impersonate.leave')}}" class="dropdown-item">
                                <i class="fa fa-user-secret"></i> Leave
                            </a>
                            <div class="dropdown-divider"></div>
                            @endImpersonating

                            @can('users.read')
                            <a href="{{route('admin.users.index')}}" class="dropdown-item @if(Route::is('admin.users.*'))active @endif">
                                <i class="fa fa-users" aria-hidden="true"></i> Users
                            </a>
                            @endcan

                            @can('invites.read')
                            <a href="{{route('admin.invites.index')}}" class="dropdown-item @if(Route::is('admin.invites.*'))active @endif">
                                <i class="fa fa-envelope" aria-hidden="true"></i> Invites
                            </a>
                            @endcan

                            @role('super-admin')
                            <a href="{{route('admin.activities.index')}}" class="dropdown-item @if(Route::is('admin.activities.*'))active @endif">
                                <i class="fa fa-keyboard-o" aria-hidden="true"></i> Activity
                            </a>
                            @endrole

                            @canany(['users.read', 'invites.read'])
                            <div class="dropdown-divider"></div>
                            @endcanany

                            <a href="{{route('vendors.index')}}" class="dropdown-item @if(Route::is('vendors.*'))active @endif">
                                <i class="fa fa-building-o"></i> Vendors
                            </a>
                            <a href="{{route('donors.index')}}" class="dropdown-item @if(Route::is('donors.*'))active @endif">
                                <i class="fa fa-user"></i> Donors
                            </a>
                            <a href="{{route('school-families.index')}}" class="dropdown-item @if(Route::is('school-families.*'))active @endif">
                                <i class="fa fa-address-book-o"></i> School Families
                            </a>
                            <div class="dropdown-divider"></div>

                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
