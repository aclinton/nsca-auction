@if(View::hasSection('subnav'))
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm d-none d-md-block">
        <div class="container">
            <ul class="navbar-nav mr-auto">
                @yield('subnav')
            </ul>
        </div>
    </nav>
@endif
