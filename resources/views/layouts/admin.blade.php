@extends('layouts.app')

@section('content')
    <h1 class="text-size-2 mb-3">Administration</h1>
    <div class="row">
        <div class="col-sm-4 col-md-3 col-lg-2">
            <div class="side-nav">
                <div class="side-nav__item">
                    <a href="{{route('admin.users.index')}}"
                       class="d-flex align-items-center @if(Route::is('admin.users.*'))active @endif">
                        <i class="fa fa-users mr-2"></i>
                        <span class="side-nav__item-text">Users</span>
                    </a>
                </div>
                {{--
                <div class="side-nav__item">
                    <a href="{{route('admin.roles.index')}}"
                       class="d-flex align-items-center @if(Route::is('admin.roles.*'))active @endif">
                        <i class="fa fa-sign-language mr-2"></i>
                        <span class="side-nav__item-text">Roles</span>
                    </a>
                </div>
                --}}
                <div class="side-nav__item">
                    <a href="{{route('admin.invites.index')}}"
                       class="d-flex align-items-center @if(Route::is('admin.invites.*'))active @endif">
                        <i class="fa fa-envelope mr-2"></i>
                        <span class="side-nav__item-text">Invites</span>
                    </a>
                </div>
                @role('super-admin')
                <div class="side-nav__item">
                    <a href="{{route('admin.activities.index')}}"
                       class="d-flex align-items-center @if(Route::is('admin.activities.*'))active @endif">
                        <i class="fa fa-keyboard-o mr-2" aria-hidden="true"></i>
                        <span class="side-nav__item-text">Activities</span>
                    </a>
                </div>
                @endrole
            </div>
        </div>
        <div class="col-sm-8 col-md-9 col-lg-10">
            @yield('rightContent')
        </div>
    </div>
@endsection
