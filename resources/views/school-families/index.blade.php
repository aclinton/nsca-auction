@extends('layouts.app')

@section('title', 'School Families | ')

@section('content')
    <div class="row mt-2">
        <div class="col">
            <div class="card nsca-card nsca-card__table">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        <h1 class="text-size-1_5 mb-0">School Families</h1>
                        @can('school-families.create')
                        <a href="{{route('school-families.create')}}" class="btn btn-primary">Add Family</a>
                        @endcan
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-hover table-responsive-xs table-nsca">
                        <thead>
                            <tr>
                                <th>Family</th>
                                <th>First Parent</th>
                                <th>Second Parent</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($families as $family)
                                <tr>
                                    <td>{{$family->family_last_name}}</td>
                                    <td>{{$family->first_parent_name}}</td>
                                    <td>{{$family->second_parent_name}}</td>
                                    <td class="text-right">
                                        <a class="btn btn-primary" href="{{route('school-families.edit', $family)}}">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
