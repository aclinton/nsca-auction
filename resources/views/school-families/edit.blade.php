@extends('layouts.app')

@section('title', 'Edit Family | ')

@section('breadcrumbs')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="{{route('school-families.index')}}">School Families</a></li>
            <li class="breadcrumb-item active" aria-current="page">Edit Family</li>
        </ol>
    </nav>
@endsection

@section('content')
    <div class="card nsca-card">
        <div class="card-header">
            <h3 class="card-title mb-0">Edit School Family</h3>
        </div>
        <div class="card-body">
            <form action="{{route('school-families.update', $family)}}" method="POST">
                @csrf
                @method('put')
                <div class="form-group row">
                    <label class="col-form-label col-sm-3 text-md-right col-md-4" for="family_last_name">Last Name</label>
                    <div class="col-sm-9 col-md-8">
                        <input
                            class="form-control @if($errors->has('family_last_name'))is-invalid @endif"
                            value="{{$family->family_last_name}}"
                            placeholder="Smith"
                            type="text"
                            name="family_last_name"
                            id="family_last_name">
                        @if($errors->has('family_last_name'))
                            <div class="invalid-feedback">
                                @foreach($errors->get('family_last_name') as $error)
                                    <div>
                                        {{$error}}
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-sm-3 text-md-right col-md-4" for="first_parent_name">First Parent</label>
                    <div class="col-sm-9 col-md-8">
                        <input
                            class="form-control @if($errors->has('first_parent_name'))is-invalid @endif"
                            value="{{$family->first_parent_name}}"
                            placeholder="John"
                            type="text"
                            name="first_parent_name"
                            id="first_parent_name">
                        @if($errors->has('first_parent_name'))
                            <div class="invalid-feedback">
                                @foreach($errors->get('first_parent_name') as $error)
                                    <div>
                                        {{$error}}
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-sm-3 text-md-right col-md-4" for="second_parent_name">Second Parent</label>
                    <div class="col-sm-9 col-md-8">
                        <input
                            class="form-control @if($errors->has('second_parent_name'))is-invalid @endif"
                            value="{{$family->second_parent_name}}"
                            placeholder="Jane"
                            type="text"
                            name="second_parent_name"
                            id="second_parent_name">
                        @if($errors->has('second_parent_name'))
                            <div class="invalid-feedback">
                                @foreach($errors->get('second_parent_name') as $error)
                                    <div>
                                        {{$error}}
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-9 col-md-8 offset-sm-3 offset-md-4">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
