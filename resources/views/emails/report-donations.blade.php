@component('mail::message')
# Donations Report

Please see the attached donations report.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
