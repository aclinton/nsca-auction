@component('mail::message')
# Hello!

You've been invited to be a part of the {{config('app.name')}} website.

@component('mail::button', ['url' => $invite->accept_url])
Accept Invitation
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
