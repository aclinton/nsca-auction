@extends('layouts.front-end')

@section('title', $auction->year . ' Vendors | ')

@section('rightContent')

    <auction-vendors
        :vendors='@json($json['vendors'])'
        :auction='@json($json['auction'])'
        :statuses='@json($vendor_statuses_json)' inline-template>
        <div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="d-flex align-items-center justify-content-between mb-3">
                        <h1 class="mb-0 text-size-1_5">
                            {{$auction->year}} Vendors
                        </h1>
                        @can('auction.vendors.register')
                            <modal-trigger target="#vendor-selection-modal">
                                <template #default="slotProps">
                                    <button
                                        data-toggle="tooltip"
                                        data-placement="bottom"
                                        title="A vendor must be registered to an auction before it can be linked with a parent."
                                        class="btn btn-primary"
                                        @click="slotProps.triggerModal">
                                        Register Vendors
                                    </button>
                                </template>
                            </modal-trigger>
                        @endcan
                    </div>

                    <div class="row justify-content-center" v-cloak>
                        <div class="col-sm-12">
                            <alert-block id="auction-vendor-info" :stateful="true" :dismissable="true">
                                <h4 class="alert-heading">Hi there!</h4>
                                <p>This page contains all of the vendors participating in the {{$auction->year}} auction.</p>
                                <p>By default, no vendors are registered to an auction and but they can be added to this auction by clicking
                                on the "Register Vendors" button over to the right.</p>
                            </alert-block>
                        </div>
                    </div>

                    <div class="card nsca-card">
                        <div class="card-header">
                            <h5 class="card-title mb-0">Filter Status</h5>
                        </div>
                        <div class="card-body">
                            <div class="card-text">
                                <div class="row" v-cloak>
                                    <div class="col-sm-4 mb-1" v-for="status in statuses">
                                        <label :for="'vendor-status-' + status.id" class="mb-0 d-flex align-items-center">
                                            <input type="checkbox" v-model="vendorStatus" :value="status.id" :id="'vendor-status-' + status.id" class="mr-1">
                                            <div :class="'mr-1 ml-3 swatch bg-nsca-' + status.slug"></div>
                                            <span>@{{status.name}}</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card nsca-card nsca-card__table mt-2">
                        <div class="card-header">
                            <div class="d-flex flex-column flex-md-row justify-content-between align-items-center">
                                <form class="mb-3 mb-md-0">
                                    <div class="form-group mb-0">
                                        <label class="mb-0" for="q">
                                            <input placeholder="Filter vendors..." type="text" class="form-control" id="q" v-model="query">
                                        </label>
                                    </div>
                                </form>
                                <div class="d-flex text-muted" v-cloak>
                                    @{{ mVendors.length }} vendors
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table table-nsca table-hover" :class="{'table-responsive-md': (mVendors && mVendors.length)}" v-cloak>
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Contact</th>
                                    <th>Location</th>
                                    <th>Family</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <template v-if="mVendors && mVendors.length">
                                    <tr :class="vendorBgStatusSlug(vendor)" v-for="vendor in mVendors">
                                        <td>@{{vendor.name}}</td>
                                        <td>@{{vendor.contact}}</td>
                                        <td>@{{vendor.location.address}}</td>
                                        <td>
                                            @{{vendor.school_family ? vendor.school_family.display_name : 'No Family'}}
                                        </td>
                                        <td>@{{vendor.status ? vendor.status.name : ''}}</td>
                                        <td>
                                            <div class="dropdown">
                                                <button class="btn btn-outline" type="button" data-toggle="dropdown">
                                                    <i class="fa fa-ellipsis-v"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <modal-trigger target="#vendor-status-update-modal">
                                                        <template #default="slotProps">
                                                            <a class="dropdown-item" href="#" @click.prevent="editStatus(vendor, slotProps.triggerModal)">Update Status</a>
                                                        </template>
                                                    </modal-trigger>
                                                    <a class="dropdown-item" :href="vendorEditUrl(vendor.edit_url)">Edit Vendor</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </template>
                                <tr v-else>
                                    <td colspan="6" v-if="!vendorsAreFiltered">No vendors registered to this auction yet.</td>
                                    <td colspan="6" v-else>No vendors match search criteria.</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <auction-vendor-selection-modal
                :auction='@json($auction)'
                :vendors='@json($unregistered_vendors)'
                id="vendor-selection-modal">
            </auction-vendor-selection-modal>
            <auction-vendor-status-edit-modal
                :auction='@json($auction)'
                :vendor-statuses='statuses'
                :school-families='@json($json['school_families'])'
                :vendor="selectedVendor"
                id="vendor-status-update-modal">
            </auction-vendor-status-edit-modal>
        </div>
    </auction-vendors>
@endsection
