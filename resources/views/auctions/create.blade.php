@extends('layouts.app')

@section('title', 'Create Auction | ')

@section('breadcrumbs')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="{{route('auctions.index')}}">Auctions</a></li>
            <li class="breadcrumb-item active" aria-current="page">Create Auction</li>
        </ol>
    </nav>
@endsection

@section('content')
    <div class="card nsca-card">
        <div class="card-header">
            <h3 class="card-title mb-0">New Auction</h3>
        </div>
        <div class="card-body">
            <form action="{{route('auctions.store')}}" method="POST">
                @csrf
                <div class="form-group row">
                    <label for="year" class="col-form-label col-sm-3 text-md-right col-md-4">Year</label>
                    <div class="col-sm-9 col-md-8">
                        <input class="form-control @if($errors->has('year'))is-invalid @endif"
                               type="number"
                               value="{{old('year')}}"
                               name="year"
                               id="year"
                               step="1">
                        @if($errors->has('year'))
                            <div class="invalid-feedback">
                                @foreach($errors->get('year') as $error)
                                    <div>
                                        {{$error}}
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="event_date" class="col-form-label col-sm-3 text-md-right col-md-4">Event Date</label>
                    <div class="col-sm-9 col-md-8">
                        <flatpickr :config="{defaultDate: '{{old('event_date')}}'}" input-class="form-control" name="event_date" id="event_date"></flatpickr>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-9 col-md-8 offset-sm-3 offset-md-4">
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
