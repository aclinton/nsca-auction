@extends('layouts.app')

@section('title', 'Auctions | ')

@section('content')
    <div class="row">
        <div class="col d-flex justify-content-between align-items-center">
            <h1 class="text-size-2">All Auctions</h1>
            <div>
                <a role="button" href="{{route('auctions.create')}}" class="btn btn-primary">Create Auction</a>
            </div>
        </div>
    </div>
    <div class="row mt-2">
        <div class="col-sm-12 col-md-3">
            <div class="card nsca-card nsca-card__table">
                <div class="card-body">
                    <div class="font-weight-bold px-3 py-2">Years</div>
                    <div class="list-group list-group-flush rounded-bottom" id="auction-list" role="tablist">
                        @forelse($auctions as $auction)
                            <a
                                href="#auction-{{$auction->id}}"
                                class="list-group-item list-group-item-action @if($loop->first)active @endif"
                                role="tab"
                                data-toggle="list"
                            >
                                {{$auction->year}}
                            </a>
                        @empty
                            <div class="list-group-item">No auctions created</div>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-9 mt-3 mt-md-0">
            <div class="tab-content">
                @forelse($auctions as $auction)
                    <div class="tab-pane fade @if($loop->first)show active @endif" id="auction-{{$auction->id}}" role="tabpanel">
                        <div class="card nsca-card">
                            <div class="card-header">
                                <div class="d-flex justify-content-between align-items-center">
                                    <h4 class="card-title">{{$auction->year}} Auction</h4>
                                    @can('auctions.read')
                                    <a href="{{route('auctions.school-families.index', $auction)}}" class="btn btn-primary d-none d-md-block">
                                        <i class="fa fa-cogs mr-2" aria-hidden="true"></i>Manage Auction</a>
                                    @endcan
                                </div>
                                <h5 class="card-subtitle text-muted">
                                    {{$auction->event_date ? $auction->event_date->format('l M jS @ g:ia') : ''}}
                                </h5>
                                @can('auctions.read')
                                <a href="{{route('auctions.vendors.index', $auction)}}" class="btn btn-primary d-block d-md-none mt-3">
                                    <i class="fa fa-cogs" aria-hidden="true"></i> Manage Auction</a>
                                @endcan
                            </div>
                            <div class="card-body">
                                <div class="card-text">
                                    <div class="row">
                                        <div class="col text-center flex-column d-flex">
                                            <div class="text-size-3">{{$auction->member_donors->count()}}</div>
                                            <div>Donors</div>
                                        </div>
                                        <div class="col text-center flex-column d-flex">
                                            <div class="text-size-3">{{$auction->participating_vendors_count}}</div>
                                            <div>Vendors</div>
                                        </div>
                                        <div class="col text-center flex-column d-flex">
                                            <div class="text-size-3">{{$auction->live_auction_items_count}}</div>
                                            <div>Live</div>
                                        </div>
                                        <div class="col text-center flex-column d-flex">
                                            <div class="text-size-3">{{$auction->silent_auction_items_count}}</div>
                                            <div>Silent</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @empty
                    <div class="tab-pane fade show active" role="tabpanel">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-text">
                                    No auctions have been created yet.
                                </div>
                            </div>
                        </div>
                    </div>
                @endforelse
            </div>
        </div>
    </div>
@endsection
