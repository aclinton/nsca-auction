@extends('layouts.front-end')

@section('title', $auction->year . ' School Families | ')

@section('rightContent')

    <div class="d-flex align-items-center justify-content-between mb-3">
        <h1 class="mb-0 text-size-1_5">{{$auction->year}} School Families</h1>
        @can('auction.school-families.register')
            <button class="btn btn-primary">Register School Families</button>
        @endcan
    </div>

    <div class="row justify-content-center" v-cloak>
        <div class="col-sm-12">
            <alert-block id="auction-school-family-info" :stateful="true" :dismissable="true">
                <h4 class="alert-heading">Hi there!</h4>
                <p>This page contains all of the school families currently participating in the {{$auction->year}} auction.</p>
                <p>By default, no families are registered for an auction and must be registered by clicking the "Register School Families"
                button over to the right.</p>
            </alert-block>
        </div>
    </div>

    <div class="row justify-content-center">
        @forelse($auction->schoolFamilies as $family)
            <div class="col-sm-12 col-md-6 mb-4">
                <div class="card nsca-card">
                    <div class="card-body">
                        <h4 class="card-title">{{$family->display_name}}</h4>
                        <div class="card-text">
                            <p class="text-muted mb-1">
                                Vendors @if($family->auctionVendors->count())({{$family->auctionVendors->count()}})@endif
                            </p>
                            @forelse($family->auctionVendors as $record)
                                <div class="d-flex align-items-center">
                                    <div class="swatch mr-1 bg-nsca-{{$record->status->sluggable_name ?? ''}}"></div>
                                    <div>{{$record->vendor->name}} ({{$record->status->name ?? 'no status'}})</div>
                                </div>
                            @empty
                                <div>No Vendor Selected</div>
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        @empty
            <div class="col-sm-10 col-md-8">
                <div class="alert alert-info" role="alert">
                    <div>No families have been registered to this auction yet.</div>
                </div>
            </div>
        @endforelse
    </div>
@endsection
