@extends('layouts.app')

@section('title', $auction->year . ' Vendors | ')

@section('content')
    <div class="card nsca-card nsca-card__table">
        <div class="card-header">
            <h3 class="card-title mb-0">{{$auction->year}} Vendors</h3>
        </div>
        <div class="card-body">
            <table class="table table-hover table-nsca table-responsive-md">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Contact</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Available</th>
                </tr>
                </thead>
                <tbody>
                @foreach($auction->vendors as $vendor)
                    <tr class="{{$vendor->pivot->schoolFamily ? 'table-secondary' : 'table-success'}}">
                        <td>
                            @if($vendor->url)
                                <a target="_blank" href="{{$vendor->url}}">
                                    {{$vendor->name}}
                                </a>
                            @else
                                {{$vendor->name}}
                            @endif
                        </td>
                        <td>{{$vendor->address}}</td>
                        <td>{{$vendor->contact_name}}</td>
                        <td>{{$vendor->email}}</td>
                        <td>{{$vendor->phone}}</td>
                        <td>{{$vendor->pivot->schoolFamily ? 'taken' : 'available'}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
