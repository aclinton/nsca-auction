@extends('layouts.front-end')

@section('title', $auction->year . ' Items | ')

@section('rightContent')

    <auction-items
        :items='@json($json["auction_items"])'
        :auction='@json($json["auction"])'
        inline-template>

        <div>
            <div class="d-flex align-items-center justify-content-between">
                <h1 class="mb-0 text-size-1_5">{{$auction->year}} Items</h1>
                <ul class="nav nav-pills" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active"
                           id="pills-items-list"
                           data-toggle="pill"
                           href="#items-list"
                           role="tab"
                           aria-controls="pills-home" aria-selected="true">
                            <i class="fa fa-th" v-if="!http.items"></i>
                            <i class="fa fa-spinner fa-spin" v-else></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-items-table" data-toggle="pill" href="#items-table" role="tab"
                           aria-controls="pills-profile" aria-selected="false">
                            <i class="fa fa-list" v-if="!http.order"></i>
                            <i class="fa fa-spinner fa-spin" v-else></i>
                        </a>
                    </li>
                </ul>

                @can('auction.auction-items.create')
                <a href="{{route('auctions.auction-items.create', $auction)}}" class="btn btn-primary">Add
                    Item</a>
                @endcan
            </div>

            <div class="row justify-content-center mt-3" v-cloak>
                <div class="col-sm-12">
                    <alert-block id="auction-items-info" :stateful="true" :dismissable="true">
                        <h4 class="alert-heading">Hi there!</h4>
                        <p>This page contains all of the auction items for the {{$auction->year}} auction.</p>
                        <p>An auction item could contain a single donation or it could be comprised of multiple donations. Get started with creating auction items
                        by clicking the "Add Item" to the right.</p>
                    </alert-block>
                </div>
            </div>

            <div class="tab-content">
                <div class="tab-pane fade show active" id="items-list">
                    <div class="row">
                        <template v-if="mItems && mItems.length" v-cloak>
                            <div class="col-sm-12 col-md-6 mb-4" v-for="item in mItems" :key="item.id">
                                <div class="card nsca-card">
                                    <div class="card-body">
                                        <div class="d-flex justify-content-between mb-3">
                                            <h4 class="card-title mb-0">
                                                #@{{ item.item_number }} @{{item.name}}
                                            </h4>
                                            <div>
                                                <i class="fa fa-bell"
                                                   data-toggle="tooltip"
                                                   data-placement="bottom"
                                                   title="Live auction item"
                                                   aria-hidden="true" v-if="item.type.isLive()"></i>

                                                <i class="fa fa-bell-slash"
                                                   data-toggle="tooltip"
                                                   data-placement="bottom"
                                                   title="Silent auction item"
                                                   aria-hidden="true" v-else></i>
                                            </div>
                                        </div>
                                        <div class="card-text">
                                            <p>@{{item.description}}</p>
                                            <p class="text-muted mb-1">Donations</p>
                                            <div v-for="donation in item.donations">
                                                <div>@{{donation.name}} <span
                                                        v-if="donation.qty !== null">(@{{donation.meta.qty}})</span>
                                                </div>
                                                <ul v-if="donation.hasMemberDonors()">
                                                    <li v-for="donor in donation.member_donors">
                                                        @{{donor.name}}
                                                    </li>
                                                </ul>
                                                <ul v-else-if="donation.hasVendorDonors()">
                                                    <li v-for="donor in donation.vendor_donors">
                                                        @{{donor.name}}
                                                    </li>
                                                </ul>
                                                <i v-else>No donors for this donation.</i>
                                            </div>

                                            <a :href="item.edit_url" class="btn btn-primary">
                                                <i class="fa fa-pencil mr-2"></i>Edit
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </template>

                        <div class="col-sm-12" v-else>
                            <div class="alert alert-info" role="alert">
                                <div>No items have been created for this auction yet.</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade show" id="items-table">
                    <div class="card nsca-card nsca-card__table" v-if="mItems && mItems.length">
                        <div class="card-body">
                            <drag-table
                                table-classes="table-hover table-nsca table--no-header"
                                :items="mItems"
                                @reordered="listUpdated">
                                <template v-slot:header>
                                    <th>#</th>
                                    <th>Type</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Donations</th>
                                    <th></th>
                                </template>
                                <template v-slot:row="itemProps">
                                    <td>@{{ itemProps.item.item_number }}</td>
                                    <td>@{{ itemProps.item.type.name }}</td>
                                    <td>@{{ itemProps.item.name }}</td>
                                    <td>@{{ itemProps.item.description }}</td>
                                    <td>
                                        <div v-for="donation in itemProps.item.donations">
                                            <div>@{{donation.name}} <span
                                                    v-if="donation.qty !== null">(@{{donation.meta.qty}})</span></div>
                                            <ul v-if="donation.hasMemberDonors()">
                                                <li v-for="donor in donation.member_donors">
                                                    @{{donor.name}}
                                                </li>
                                            </ul>
                                            <ul v-else-if="donation.hasVendorDonors()">
                                                <li v-for="donor in donation.vendor_donors">
                                                    @{{donor.name}}
                                                </li>
                                            </ul>
                                            <i v-else>No donors for this donation.</i>
                                        </div>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <i class="fa fa-bars cursor-grab" aria-hidden="true"></i>
                                    </td>
                                </template>
                            </drag-table>
                        </div>
                    </div>
                    <div class="alert alert-info" role="alert" v-else>
                        <div>No items have been created for this auction yet.</div>
                    </div>
                </div>
            </div>
        </div>
    </auction-items>
@endsection
