@extends('layouts.front-end')

@section('title', 'Edit Item | ')

@section('rightContent')

    <auction-item
        :donations='@json($json["donations"])'
        :form-input='@json(session()->getOldInput())'
        :form-donations='@json($json["item_donations"])'
        :form-qtys='@json($json["item_donation_qtys"])'
        :form-errors='@json($errors->toArray())'
        inline-template>
        <div>
            <div class="card nsca-card">
                <div class="card-header">
                    <h3 class="card-title mb-0">Edit Auction Item</h3>
                </div>
                <div class="card-body">
                    <div class="card-text">
                        <form action="{{route('auctions.auction-items.update', ['auction' => $auction, 'auction_item' => $auction_item])}}" method="POST">
                            @method('put')
                            @csrf
                            <div class="form-group row">
                                <label for="item_type" class="col-form-label col-sm-3 text-md-right col-md-4">Type</label>
                                <div class="col-sm-9 col-md-8">
                                    <select name="type"
                                            id="item_type" class="form-control @if($errors->has('type'))is-invalid @endif">
                                        <option value="">-- Select type --</option>
                                        @foreach($types as $type)
                                            <option value="{{$type->id}}" @if((old('type') ? old('type') : $auction_item->type->id) == $type->id)selected @endif>{{ucfirst($type->name)}}</option>
                                        @endforeach
                                    </select>

                                    @if($errors->has('type'))
                                        <div class="invalid-feedback">
                                            @foreach($errors->get('type') as $error)
                                                <div>
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="item_name" class="col-form-label col-sm-3 text-md-right col-md-4">Name</label>
                                <div class="col-sm-9 col-md-8">
                                    <input type="text" value="{{old('name') ? old('name') : $auction_item->name}}"
                                           placeholder="Lawn Care"
                                           name="name"
                                           id="item_name"
                                           class="form-control @if($errors->has('name'))is-invalid @endif">

                                    @if($errors->has('name'))
                                        <div class="invalid-feedback">
                                            @foreach($errors->get('name') as $error)
                                                <div>
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="item_number" class="col-form-label col-sm-3 text-md-right col-md-4">Item Number</label>
                                <div class="col-sm-9 col-md-8">
                                    <input type="number"
                                           placeholder="001"
                                           name="item_number"
                                           min="1"
                                           value="{{old('item_number') ? old('item_number') : $auction_item->item_number}}"
                                           step="1"
                                           id="item_number"
                                           class="form-control @if($errors->has('item_number'))is-invalid @endif">

                                    @if($errors->has('item_number'))
                                        <div class="invalid-feedback">
                                            @foreach($errors->get('item_number') as $error)
                                                <div>
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <input type="hidden" name="donations[]" v-for="donation in selectedDonations" :value="donation.id">
                                <label for="item_donations" class="col-form-label col-sm-3 text-md-right col-md-4">Donations</label>
                                <div class="col-sm-9 col-md-8">
                                    <template v-if="selectedDonations.length">
                                        <div class="row mb-2" v-for="(donation, index) in selectedDonations">
                                            <div class="col-sm-8">
                                                <div><span>@{{ donation.name }}</span><span class="text-muted" v-if="donation.value"> - $@{{ donation.value }}</span></div>
                                                <div class="text-muted" v-if="donation.description">@{{ donation.description }}</div>
                                                <div class="text-muted" v-else>No donation description available.</div>
                                                <ul v-if="donation.hasMemberDonors()">
                                                    <li v-for="member in donation.member_donors">@{{ member.name }}</li>
                                                </ul>
                                                <ul v-else-if="donation.hasVendorDonors()">
                                                    <li v-for="vendor in donation.vendor_donors">@{{ vendor.name }}</li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-4">
                                                <input
                                                        v-show="donation.qty !== null"
                                                       class="form-control"
                                                       :class="{'is-invalid': hasErrors('qtys.' + index)}"
                                                       placeholder="qty"
                                                       :id="'item_donations_' + donation.id"
                                                       type="number"
                                                       min="0"
                                                       :max="getEditDonationQtyMax(index, donation)"
                                                       step="1"
                                                       :value="getOldQty(index)"
                                                       name="qtys[]">
                                                <div class="invalid-feedback" v-if="hasErrors('qtys.' + index)">
                                                    <div v-for="e in getErrors('qtys.' + index)">
                                                        @{{e}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </template>
                                    <div class="alert alert-info mb-0" role="alert" v-else>
                                        No donations have been added yet.
                                    </div>
                                    @if($errors->has('donations'))
                                        <div class="invalid-feedback d-block mb-2">
                                            @foreach($errors->get('donations') as $error)
                                                <div>
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif
                                    <div class="mt-3">
                                        <modal-trigger target="#donation-selection-modal">
                                            <template #default="slotProps">
                                                <button class="btn btn-primary"
                                                        @click="slotProps.triggerModal"
                                                        type="button">Add Donations</button>
                                            </template>
                                        </modal-trigger>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="item_description" class="col-form-label col-sm-3 text-md-right col-md-4">Description</label>
                                <div class="col-sm-9 col-md-8">
                                <textarea name="description"
                                          placeholder="Describe the auction item..."
                                          id="item_description"
                                          rows="4"
                                          class="form-control @if($errors->has('description'))is-invalid @endif">{{$auction_item->description}}</textarea>

                                    @if($errors->has('description'))
                                        <div class="invalid-feedback">
                                            @foreach($errors->get('description') as $error)
                                                <div>
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-9 col-md-8 offset-sm-3 offset-md-4">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <auction-donation-selection-modal
                :donations="donations"
                :selected-donations="form.donations"
                @update-donation-selection="saveNewDonations"
                modal-id="donation-selection-modal">
            </auction-donation-selection-modal>
        </div>
    </auction-item>
@endsection
