@extends('layouts.front-end')

@section('title', $auction->year . ' Donations | ')

@section('rightContent')
    <div class="row justify-content-center" v-cloak>
        <div class="col-sm-12">
            <alert-block id="auction-donations-info" :stateful="true" :dismissable="true">
                <h4 class="alert-heading">Hi there!</h4>
                <p>This page contains all of the donations for the {{$auction->year}} auction.</p>
                <p>These donations can be from vendors or from church members. To add a new donation, just click the
                    "Add Donation"
                    button to the right.</p>
            </alert-block>
        </div>
    </div>

    @livewire('donations-table', ['auction' => $auction])
@endsection
