@extends('layouts.front-end')

@section('title', 'Edit Donation | ')

@section('rightContent')

    <auction-donations
        :donors='@json($json['donors'])'
        :donation-members='@json($json['selected_members'])'
        donor-type="{{$donor_type}}" inline-template>
        <div>
            <div class="card nsca-card">
                <div class="card-header">
                    <h3 class="card-title mb-0">Update Auction Donation</h3>
                </div>
                <div class="card-body">
                    <div class="card-text mt-4">
                        <form action="{{route('auctions.donations.update', ['auction' => $auction, 'donation' => $donation])}}" method="POST">
                            @method('put')
                            @csrf
                            <div class="form-group row">
                                <label for="donation_donor_type" class="col-form-label col-sm-3 col-md-4 text-md-right">Donor
                                    Type</label>
                                <div class="col-sm-9 col-md-8">
                                    <select name="donation_donor_type" v-model="form.donorType"
                                            id="donation_donor_type" class="form-control">
                                        <option value="">-- Select Donor Type --</option>
                                        <option value="vendor">Vendor</option>
                                        <option value="member">Church Member</option>
                                    </select>
                                    @if($errors->has('donation_donor_type'))
                                        <div class="invalid-feedback">
                                            @foreach($errors->get('donation_donor_type') as $error)
                                                <div>
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="donation_type" class="col-form-label col-sm-3 col-md-4 text-md-right">Donation Type</label>
                                <div class="col-sm-9 col-md-8">
                                    <select name="donation_type" id="donation_type"
                                            class="form-control @if($errors->has('donation_type'))is-invalid @endif">
                                        <option value="">-- Select Donation Type --</option>
                                        @foreach($donation_types as $type)
                                            <option value="{{$type->id}}" @if($donation->type->id == $type->id)selected @endif>{{ucfirst($type->name)}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('donation_type'))
                                        <div class="invalid-feedback">
                                            @foreach($errors->get('donation_type') as $error)
                                                <div>
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="vendor" v-if="isVendorDonor">
                                <div class="form-group row">
                                    <label for="vendor" class="col-form-label col-sm-3 col-md-4 text-md-right">Vendor</label>
                                    <div class="col-sm-9 col-md-8">
                                        <select name="vendor" id="vendor"
                                                class="form-control @if($errors->has('vendor'))is-invalid @endif">
                                            <option value="">-- Select Vendor --</option>
                                            @foreach($auction->vendors as $vendor)
                                                <option value="{{$vendor->id}}" @if(($donation->vendorDonors->first()->id ?? null) == $vendor->id)selected @endif>{{$vendor->name}}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('vendor'))
                                            <div class="invalid-feedback">
                                                @foreach($errors->get('vendor') as $error)
                                                    <div>
                                                        {{$error}}
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="member" v-if="isMemberDonor">
                                <div class="form-group row">
                                    <label for="donor" class="col-form-label col-sm-3 col-md-4 text-md-right">Church Member(s)</label>
                                    <div class="col-sm-9 col-md-8">
                                        {{-- Dynamically keep track of added donation members --}}
                                        <input type="hidden" name="members[]" v-for="id in form.members" :value="id">

                                        <div class="pill-container" v-if="selectedMembers.length">
                                            <div v-for="member in selectedMembers" class="pill-item">
                                                <div class="pill-item__content">@{{member.name}}</div>
                                                <div class="pill-item__close" @click="removeMember(member.id)"><span>x</span></div>
                                            </div>
                                        </div>
                                        <div class="alert alert-info" role="alert" v-else>
                                            No donors have been added yet.
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-9 col-md-8 offset-sm-3 offset-md-4">
                                        <div class="d-flex">
                                            <modal-trigger target="#donor-selection-modal">
                                                <template #default="slotProps">
                                                    <button class="btn btn-primary"
                                                            @click="slotProps.triggerModal"
                                                            type="button">Add Donors
                                                    </button>
                                                </template>
                                            </modal-trigger>
                                            <modal-trigger target="#create-donor-modal">
                                                <template #default="slotProps">
                                                    <button class="btn btn-primary ml-1" type="button"
                                                            @click="slotProps.triggerModal">Create Donor
                                                    </button>
                                                </template>
                                            </modal-trigger>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-1">
                                    <div class="col-sm-9 col-md-8 offset-sm-3 offset-md-4">
                                        <i class="text-muted">If you don't see a donor when adding a donor, you can create a new one by clicking the <b>Create Donor</b> button.</i>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-form-label col-sm-3 col-md-4 text-md-right">Name</label>
                                <div class="col-sm-9 col-md-8">
                                    <input type="text" value="{{$donation->name}}"
                                           class="form-control @if($errors->has('name'))is-invalid @endif"
                                           placeholder="babysitting" name="name" id="name">
                                    @if($errors->has('name'))
                                        <div class="invalid-feedback">
                                            @foreach($errors->get('name') as $error)
                                                <div>
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="qty" class="col-form-label col-sm-3 col-md-4 text-md-right">Quantity</label>
                                <div class="col-sm-9 col-md-8">
                                    <input type="number" value="{{$donation->qty}}" step="1"
                                           class="form-control @if($errors->has('qty'))is-invalid @endif"
                                           name="qty" id="qty">
                                    @if($errors->has('qty'))
                                        <div class="invalid-feedback">
                                            @foreach($errors->get('qty') as $error)
                                                <div>
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="value" class="col-form-label col-sm-3 col-md-4 text-md-right">Value</label>
                                <div class="col-sm-9 col-md-8">
                                    <input type="number" value="{{$donation->value}}" step="1" placeholder="$50"
                                           class="form-control @if($errors->has('value'))is-invalid @endif"
                                           name="value" id="value">
                                    @if($errors->has('value'))
                                        <div class="invalid-feedback">
                                            @foreach($errors->get('value') as $error)
                                                <div>
                                                    {{$error}}
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="description" class="col-form-label col-sm-3 col-md-4 text-md-right">Description</label>
                                <div class="col-sm-9 col-md-8">
                                            <textarea placeholder="Good for one babysitting session"
                                                      class="form-control" name="description" id="description"
                                                      rows="4">{{$donation->description}}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-9 col-md-8 offset-sm-3 offset-md-4">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <donation-donor-selection-modal
                :donors="mDonors"
                :selected-donors="form.members"
                @update-donor-selection="saveNewMembers"
                modal-id="donor-selection-modal">
            </donation-donor-selection-modal>

            <new-donor-modal
                @created-donor="fetchDonors"
                modal-id="create-donor-modal">
            </new-donor-modal>
        </div>
    </auction-donations>
@endsection

