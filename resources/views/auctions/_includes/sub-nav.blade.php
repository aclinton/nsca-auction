@section('subnav')
    <li class="nav-item">
        <a class="nav-link @if(Request::routeIs('auctions.school-families.*'))active @endif"
           href="{{route('auctions.school-families.index', $auction)}}">School Families</a>
    </li>
    <li class="nav-item">
        <a class="nav-link @if(Request::routeIs('auctions.vendors.*'))active @endif"
           href="{{route('auctions.vendors.index', $auction)}}">Vendors</a>
    </li>
    <li class="nav-item">
        <a class="nav-link @if(Request::routeIs('auctions.donations.*'))active @endif"
           href="{{route('auctions.donations.index', $auction)}}">Donations</a>
    </li>
    <li class="nav-item">
        <a class="nav-link @if(Request::routeIs('auctions.auction-items.*'))active @endif"
           href="{{route('auctions.auction-items.index', $auction)}}">Auction Items</a>
    </li>
@endsection
