@extends('layouts.app')

@section('title', 'Donors | ')

@section('content')
    <div class="row mt-2">
        <div class="col">
            <div class="card nsca-card nsca-card__table">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        <h1 class="text-size-1_5 mb-0">Donors</h1>
                        @can('donors.create')
                        <a href="{{route('donors.create')}}" class="btn btn-primary">Add Donor</a>
                        @endcan
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-hover table-responsive-lg table-nsca">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($donors as $donor)
                                <tr>
                                    <td>{{$donor->name}}</td>
                                    <td>{{$donor->email}}</td>
                                    <td>{{$donor->phone}}</td>
                                    <td class="text-right">
                                        @can('donors.update')
                                        <a href="{{route('donors.edit', $donor)}}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
