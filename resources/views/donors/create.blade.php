@extends('layouts.app')

@section('title', 'New Donor | ')

@section('breadcrumbs')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="{{route('donors.index')}}">Donors</a></li>
            <li class="breadcrumb-item active" aria-current="page">Create Donor</li>
        </ol>
    </nav>
@endsection

@section('content')
    <div class="card nsca-card">
        <div class="card-header">
            <h3 class="card-title mb-0">New Donor</h3>
        </div>
        <div class="card-body">
            <form action="{{route('donors.store')}}" method="POST">
                @csrf
                <div class="form-group row">
                    <label class="col-form-label col-sm-3 text-md-right col-md-4" for="name">Name</label>
                    <div class="col-sm-9 col-md-8">
                        <input
                            class="form-control @if($errors->has('name'))is-invalid @endif"
                            value="{{old('name')}}"
                            placeholder="Homestar Runner"
                            type="text"
                            name="name"
                            id="name">
                        @if($errors->has('name'))
                            <div class="invalid-feedback">
                                @foreach($errors->get('name') as $error)
                                    <div>
                                        {{$error}}
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-sm-3 text-md-right col-md-4" for="email">Email</label>
                    <div class="col-sm-9 col-md-8">
                        <input
                            class="form-control @if($errors->has('email'))is-invalid @endif"
                            value="{{old('email')}}"
                            placeholder="gmail@aol.com"
                            type="email"
                            name="email"
                            id="email">
                        @if($errors->has('email'))
                            <div class="invalid-feedback">
                                @foreach($errors->get('email') as $error)
                                    <div>
                                        {{$error}}
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-sm-3 text-md-right col-md-4" for="phone">Phone</label>
                    <div class="col-sm-9 col-md-8">
                        <input
                            class="form-control @if($errors->has('phone'))is-invalid @endif"
                            value="{{old('phone')}}"
                            placeholder="(123)456-1234"
                            type="tel"
                            name="phone"
                            id="phone">
                        @if($errors->has('phone'))
                            <div class="invalid-feedback">
                                @foreach($errors->get('phone') as $error)
                                    <div>
                                        {{$error}}
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-9 col-md-8 offset-sm-3 offset-md-4">
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
