@extends('layouts.admin')

@section('title', 'Invites | ')

@section('rightContent')
    <div class="card nsca-card nsca-card__table">
        <div class="card-header">
            <div class="d-flex justify-content-between align-items-center">
                <h4 class="mb-0">System Invites</h4>
                <a href="{{route('admin.invites.create')}}" class="btn btn-primary">Create Invite</a>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-nsca">
                <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th>Email</th>
                    <th>Sent</th>
                    <th>Accepted</th>
                    <th>Roles</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @forelse($invites as $invite)
                    <tr>
                        <td class="text-center">{{$invite->id}}</td>
                        <td>{{$invite->email}}</td>
                        <td>{{$invite->sent ? 'yes' : 'no'}}</td>
                        <td>{{$invite->accepted ? 'yes' : 'no'}}</td>
                        <td>{{$invite->roles->pluck('name')->implode(', ')}}</td>
                        <td class="text-center">
                            @if(!$invite->accepted)
                                <button class="btn btn-outline-primary"
                                        onclick="event.preventDefault();
                                                     document.getElementById('resend-invite-{{$invite->id}}').submit();">
                                    <i class="fa fa-paper-plane"></i>
                                </button>

                                <form
                                    class="d-none"
                                    id="resend-invite-{{$invite->id}}"
                                    method="POST"
                                    action="{{route('resend-invite', $invite)}}">
                                    @csrf
                                </form>
                            @endif
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5">No invites have been created yet.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection
