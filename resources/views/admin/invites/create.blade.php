@extends('layouts.admin')

@section('title', 'Create Invite | ')

@section('rightContent')
    <div class="card nsca-card">
        <div class="card-header">
            <h4 class="mb-0">New Invite</h4>
        </div>
        <div class="card-body">
            <form action="{{route('admin.invites.store')}}" method="POST">
                @csrf

                <div class="form-group row">
                    <label class="col-form-label col-sm-3 text-md-right" for="email">Email</label>
                    <div class="col-sm-9">
                        <input
                            class="form-control @if($errors->has('email'))is-invalid @endif"
                            value="{{old('email')}}"
                            placeholder="john@doe.com"
                            type="email"
                            name="email"
                            id="email">
                        @if($errors->has('email'))
                            <div class="invalid-feedback">
                                @foreach($errors->get('email') as $error)
                                    <div>
                                        {{$error}}
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-sm-3 text-md-right" for="roles">Roles</label>
                    <div class="col-sm-9">
                        <div class="row">
                            @foreach($roles as $role)
                                <div class="col-sm-4">
                                    <label for="role-{{$role->id}}">
                                        <input type="checkbox" value="{{$role->id}}" id="role-{{$role->id}}" name="roles[]" @if(in_array($role->id, old('roles', [])))checked @endif>
                                        {{ucfirst($role->name)}}
                                    </label>
                                </div>
                            @endforeach
                        </div>

                        @if($errors->has('roles'))
                            <input type="hidden" class="is-invalid" name="_roles_error" value="">

                            <div class="invalid-feedback">
                                @foreach($errors->get('roles') as $error)
                                    <div>
                                        {{$error}}
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-9 offset-md-3">
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
