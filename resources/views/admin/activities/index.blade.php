@extends('layouts.admin')

@section('title', 'User Activity | ')

@section('rightContent')
    <div class="card nsca-card">
        <div class="card-header">
            <h3 class="card-title mb-0">Filter</h3>
        </div>
        <div class="card-body">

        </div>
    </div>
    <div class="card nsca-card nsca-card__table mt-4">
        <div class="card-header">
            <div class="d-flex justify-content-between align-items-center">
                <h3 class="card-title mb-0">Activities</h3>
                <div class="text-muted">
                    Page {{request()->get('page', 1)}} of {{$activities->lastPage()}} | Total {{$activities->total()}}
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-nsca table-responsive-lg">
                <thead>
                    <tr>
                        <th>Description</th>
                        <th>User</th>
                        <th>Target</th>
                        <th>IP</th>
                        <th>Action</th>
                        <th>Timestamp</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($activities as $activity)
                        <tr>
                            <td>{{$activity->description}}</td>
                            <td>{{$activity->causer ? $activity->causer->name : 'incognito'}}</td>
                            <td>{{$activity->subject ? get_class($activity->subject) : ''}}</td>
                            <td>{{$activity->getExtraProperty('ip')}}</td>
                            <td>
                                <div>{{$activity->getExtraProperty('url')}}</div>
                                <div>{{$activity->getExtraProperty('method')}}</div>
                            </td>
                            <td>{{$activity->created_at->format('D, M jS @ g:i - o')}}</td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="6">
                            <div class="d-flex justify-content-center">
                                {{$activities->render()}}
                            </div>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection
