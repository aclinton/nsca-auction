@extends('layouts.admin')

@section('title', 'Edit User | ')

@section('rightContent')
    <div class="card nsca-card mt-4 mt-md-0">
        <div class="card-header">
            <h4 class="card-title mb-0">Profile</h4>
        </div>
        <div class="card-body">
            <form action="{{route('admin.users.update', $user)}}" method="POST">
                @csrf
                @method('put')
                <div class="form-group row">
                    <label class="col-form-label col-sm-3 text-md-right" for="name">Name</label>
                    <div class="col-sm-9">
                        <input
                            class="form-control @if($errors->has('name'))is-invalid @endif"
                            value="{{old('name') ? old('name') : $user->name}}"
                            placeholder="John"
                            type="text"
                            name="name"
                            id="name">
                        @if($errors->has('name'))
                            <div class="invalid-feedback">
                                @foreach($errors->get('name') as $error)
                                    <div>
                                        {{$error}}
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-sm-3 text-md-right" for="email">Email</label>
                    <div class="col-sm-9">
                        <input
                            class="form-control @if($errors->has('email'))is-invalid @endif"
                            value="{{old('email') ? old('email') : $user->email}}"
                            placeholder="gmail@aol.com"
                            type="email"
                            name="email"
                            id="email">
                        @if($errors->has('email'))
                            <div class="invalid-feedback">
                                @foreach($errors->get('email') as $error)
                                    <div>
                                        {{$error}}
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-sm-3 text-md-right" for="name">Roles</label>
                    <div class="col-sm-9">
                        <div class="row">
                            @foreach($roles as $role)
                                <div class="col-sm-4">
                                    <label for="role-{{$role->id}}">
                                        <input type="checkbox" value="{{$role->id}}" id="role-{{$role->id}}" name="roles[]" @if($user->roles->contains($role->id))checked @endif>
                                        {{ucfirst($role->name)}}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-9 offset-3">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
