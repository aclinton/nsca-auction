@extends('layouts.admin')

@section('title', 'Users | ')

@section('rightContent')
<div class="card nsca-card nsca-card__table">
    <div class="card-header">
        <h4 class="mb-0">Current Users</h4>
    </div>
    <div class="card-body">
        <table class="table table-nsca">
            <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Roles</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                    <tr>
                        <td class="text-center">{{$user->id}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->roles->pluck('name')->implode(', ')}}</td>
                        <td class="text-center">
                            <a class="btn btn-outline-primary" href="{{route('admin.users.edit', $user)}}">
                                <i class="fa fa-pencil"></i>
                            </a>
                            @if(!$user->is(auth()->user()))
                            <a href="{{route('impersonate', $user->id)}}" class="btn btn-outline-primary">
                                <i class="fa fa-user-secret"></i>
                            </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
