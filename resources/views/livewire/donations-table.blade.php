<div class="card nsca-card nsca-card__table">
    <div class="card-header">
        <div class="d-flex justify-content-between align-items-center">
            <h1 class="mb-0 text-size-1_5">{{$auction->year}} Donations</h1>
            @can('auction.donations.create')
                <a href="{{route('auctions.donations.create', $auction)}}" class="btn btn-primary">Add Donation</a>
            @endcan
        </div>
        <div class="mt-4">
            <div class="row d-flex align-items-center">
                <div class="col-sm-4">
                    <input wire:model.debounce.300ms="query" placeholder="Search donations..." name="query" type="text" class="form-control">
                </div>
                <div class="col-sm-4 mt-4 mt-sm-0">
                    <select class="form-control" name="filter_donor_type" wire:model="filter_donor_type">
                        <option value="">-- Donor Type --</option>
                        <option value="vendor">Vendor</option>
                        <option value="member">Church Member</option>
                    </select>
                </div>
                <div class="col-sm-4 mt-4 mt-sm-0">
                    <select class="form-control" name="filter_donation_type" wire:model="filter_donation_type">
                        <option value="">-- Donation Type --</option>
                        <option value="good">Good</option>
                        <option value="food">Food</option>
                        <option value="service">Service</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="position-absolute w-100 h-100 table-loader" wire:loading>
            <div class="position-relative text-center mt-4">
                <i class="fa fa-spin fa-spinner fa-5x"></i>
            </div>
        </div>
        <table class="table table-hover table-nsca @if($donations->count())table-responsive-lg @endif">
            <thead>
            <tr>
                <th></th>
                <th>Name</th>
                <th>Type</th>
                <th>Accepted</th>
                <th>Description</th>
                <th>Qty</th>
                <th>Value</th>
                <th>Donors</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @forelse($donations as $donation)
                <tr>
                    <td>{{$loop->index + 1}}</td>
                    <td>{{$donation->name}}</td>
                    <td>{{$donation->type->name ?? ''}}</td>
                    <td class="text-center">@if($donation->accepted)<i class="fa fa-check text-success"></i> @else
                            <i class="fa fa-times"></i> @endif</td>
                    <td>{{$donation->description}}</td>
                    <td>{{$donation->qty}}</td>
                    <td>{{$donation->value ? "$" . number_format($donation->value, 2, '.', ',') : ''}}</td>
                    <td>
                        @if($donation->vendorDonors->count())
                            @foreach($donation->vendorDonors as $vendor_donor)
                                <span>{{$vendor_donor->name}} - <i>{{$vendor_donor->auctions->first()->pivot->schoolFamily->family_last_name ?? 'no family'}}</i></span>@if(!$loop->last), @endif
                            @endforeach
                        @elseif($donation->memberDonors->count())
                            @foreach($donation->memberDonors as $member_donor)
                                <span>{{$member_donor->first_name}}</span>@if(!$loop->last), @endif
                            @endforeach
                        @else
                            No donor specified
                        @endif
                    </td>
                    <td>
                        <div class="dropdown">
                            <button class="btn btn-outline" type="button" data-toggle="dropdown">
                                <i class="fa fa-ellipsis-v"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a
                                    href="{{route('auctions.donations.edit', ['auction' => $auction, 'donation' => $donation])}}"
                                    class="dropdown-item">
                                    <i class="fa fa-pencil"></i> Edit
                                </a>
                                @can('auction.donations.approve')
                                    @if(!$donation->accepted)
                                        <a href="#" class="dropdown-item" onclick="event.preventDefault();
                                            document.getElementsByClassName('approve-form-{{$donation->id}}')[0].submit();">
                                            <i class="fa fa-check"></i> Approve
                                        </a>
                                    @else
                                        <a href="#" class="dropdown-item" onclick="event.preventDefault();
                                            document.getElementsByClassName('reject-form-{{$donation->id}}')[0].submit();">
                                            <i class="fa fa-times"></i> Reject
                                        </a>
                                    @endif
                                @endcan
                            </div>
                        </div>

                        @can('auction.donations.approve')
                            <form class="approve-form-{{$donation->id}}"
                                  action="{{ route('donation-approve', $donation) }}" method="POST"
                                  style="display: none;">
                                @csrf
                            </form>

                            <form class="reject-form-{{$donation->id}}"
                                  action="{{ route('donation-reject', $donation) }}" method="POST"
                                  style="display: none;">
                                @csrf
                            </form>
                        @endcan
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="8">No donations made for this auction yet.</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>
