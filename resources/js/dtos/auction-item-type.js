import Dto from "./dto";

export default class AuctionItemType extends Dto {
    constructor(attrs) {
        super();

        let fields = [
            'id',
            'name'
        ];

        this.make(attrs, fields);
    }

    isLive() {
        return this.name === 'live';
    }

    isSilent() {
        return this.name === 'silent';
    }
}
