import Dto from "./dto";

export default class Donor extends Dto {
    constructor(attrs = {}) {
        super();

        let fields = [
            'id',
            'first_name',
            'last_name',
            'name',
            'email',
            'phone'
        ];

        this.make(attrs, fields);
    }

    fullNameReverse() {
        let parts = [this.last_name, this.first_name];

        return parts.filter(p => p).join(', ');
    }
}
