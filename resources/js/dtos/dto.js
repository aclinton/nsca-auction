import {titleize} from 'underscore.string';

export default class Dto {
    make(attrs, fields) {
        fields.map(key => {
            if(attrs.hasOwnProperty(key)) {
                let method = `set${titleize(key)}Attribute`;
                let val = attrs[key];

                if(typeof this[method] === 'function') {
                    val = this[method](val);
                }

                this[key] = val;
            }
        });
    }
}
