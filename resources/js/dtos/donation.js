import _ from 'lodash';
import Dto from "./dto";

export default class Donation extends Dto {
    constructor(attrs = {}) {
        super();

        let fields = [
            'accepted',
            'description',
            'id',
            'name',
            'qty',
            'qty_available',
            'type',
            'value',
            'vendor_donors',
            'member_donors',
            'meta'
        ];

        this.make(attrs, fields);
    }

    /**
     *
     * @returns {*|boolean}
     */
    hasMemberDonors() {
        return this.member_donors && this.member_donors.length > 0;
    }

    /**
     *
     * @returns {*|boolean}
     */
    hasVendorDonors() {
        return this.vendor_donors && this.vendor_donors.length > 0;
    }
}
