import Dto from "./dto";
import Donation from "./donation";
import AuctionItemType from "./auction-item-type";

export default class AuctionItem extends Dto {
    constructor(attrs = {}) {
        super();

        let fields = [
            'id',
            'name',
            'item_number',
            'description',
            'accepted',
            'qty',
            'value',
            'auction',
            'type',
            'donations',
            'edit_url'
        ];

        this.make(attrs, fields);
    }

    setDonationsAttribute(donations) {
        return donations.map(d => new Donation(d));
    }

    setTypeAttribute(type) {
        return new AuctionItemType(type);
    }
}
