<template>
    <modal :id="modalId" size="lg" :scrollable="true">
        <template #default="slotProps">
            <div class="modal-header">
                <h5 class="modal-title">Choose Donors</h5>
                <button @click="slotProps.closeModal" type="button" class="close" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body modal-table">
                <table class="table table-hover table-nsca">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>ID</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="donor in sortedDonors">
                        <td><input type="checkbox" v-model="modalDonors" :value="donor.id"></td>
                        <td>{{donor.fullNameReverse()}}</td>
                        <td>{{donor.email}}</td>
                        <td>{{donor.phone}}</td>
                        <td>{{donor.id}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button class="btn" type="button" @click="slotProps.closeModal">Cancel</button>
                <button class="btn btn-primary" @click="updateSelection(slotProps.closeModal)">Update</button>
            </div>
        </template>
    </modal>
</template>

<script>
    export default {
        props: {
            modalId: {
                required: true
            },
            donors: {
                required: true,
                type: Array
            },
            selectedDonors: {
                required: true,
                type: Array
            }
        },
        data() {
            return {
                modalDonors: this.selectedDonors
            }
        },
        watch: {
            selectedDonors(donors) {
                if(donors && donors.length) {
                    this.modalDonors = donors;
                }
            }
        },
        methods: {
            updateSelection(cb) {
                this.$emit('update-donor-selection', this.modalDonors);
                this.modalDonors = [];

                cb();
            }
        },
        computed: {
            sortedDonors() {
                return _.sortBy(this.donors, (o) => {
                    return o.fullNameReverse();
                });
            }
        }
    }
</script>
