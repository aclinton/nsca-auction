import Vue from 'vue';
import donations from "../../../dtos/donation";
import _ from 'lodash';

Vue.component('auction-item', {
    props: {
        donations: {
            required: true,
            type: Array,
            default() {
                return []
            }
        },
        formDonations: {
            type: Array,
            default() {
                return [];
            }
        },
        formQtys: {
            type: Array,
            default() {
                return [];
            }
        },
        formInput: {},
        formErrors: {
            type: Object,
            default() {
                return {};
            }
        }
    },
    data() {
        return {
            form: {
                donations: [],
                description: null,
                name: null
            },
            formCache: {
                name: null,
                description: null
            },
            useDonationDescription: false,
            useDonationName: false
        }
    },
    watch: {
        useDonationDescription(val) {
            if (val) {
                let donation = this.selectedDonations[0];

                this.formCache.description = this.form.description;
                this.form.description = donation.description;
            } else {
                this.form.description = this.formCache.description;
            }
        },
        useDonationName(val) {
            if (val) {
                let donation = this.selectedDonations[0];

                this.formCache.name = this.form.name;
                this.form.name = donation.name;
            } else {
                this.form.name = this.formCache.name;
            }
        }
    },
    computed: {
        selectedDonations() {
            let donations = [];

            this.form.donations.forEach(i => {
                let d = this.mDonations.find(d => d.id === i);

                if (d !== undefined) {
                    donations.push(d);
                }
            });

            return donations;
        },
        mDonations() {
            return this.donations.map(d => {
                return new donations(d);
            });
        }
    },
    created() {
        if (this.formInput) {
            this.form.name = this.formInput.name || null;
            this.form.description = this.formInput.description || null;

            if(this.formInput.donations && this.formInput.donations.length) {
                this.form.donations = this.formInput.donations.map(id => parseInt(id));
            } else {
                this.form.donations = this.formDonations.map(id => parseInt(id));
            }
        }
    },
    methods: {
        saveNewDonations(ids) {
            this.form.donations = this.form.donations.concat(ids);
        },
        hasErrors(name) {
            return name in this.formErrors;
        },
        getErrors(name) {
            return this.formErrors[name];
        },
        getOldQty(index) {
            return _.get(this.formInput, `qtys[${index}]`) || this.formQtys[index] || null;
        },
        getEditDonationQtyMax(index, donation) {
            let qty = this.getOldQty(index);

            if (qty === null) {
                return null;
            }

            return donation.qty_available + qty;
        },
        removeDonation(donation) {
            this.form.donations = this.form.donations.filter(id => {
                return id !== donation.id;
            });
        }
    }
});
