import Vue from 'vue';
import Donor from "../../../dtos/donor";

Vue.component('auction-donations', {
    props: {
        donorType: {
            type: String,
            default: null
        },
        donors: {
            type: Array,
            default() {
                return [];
            }
        },
        donationMembers: {
            type: Array,
            default() {
                return [];
            }
        }
    },
    data() {
        return {
            http: {
                donors: false
            },
            form: {
                donorType: this.donorType,
                members: []
            },
            mDonors: []
        };
    },
    computed: {
        isVendorDonor() {
            return this.form.donorType === 'vendor';
        },
        isMemberDonor() {
            return this.form.donorType === 'member';
        },
        selectedMembers() {
            return this.mDonors.filter(d => {
                let index = this.form.members.indexOf(d.id);

                return index > -1;
            });
        }
    },
    created() {
        this.form.members = this.donationMembers.map(m => m.id);
        this.setDonors(this.donors);
    },
    methods: {
        setDonors(donors) {
            this.mDonors = donors.map(d => {
                return new Donor(d);
            });
        },
        saveNewMembers(members) {
            this.form.members = this.form.members.concat(members);
        },
        fetchDonors(callbackData) {
            this.http.donors = true;
            let targetName = null;

            if(callbackData && callbackData.name) {
                targetName = callbackData.name;
            }

            this.$http.get('/ajax/donors').then(res => {
                this.setDonors(res.data.data);

                if(targetName) {
                    let donors = this.mDonors.filter(d => {
                        return d.name === targetName;
                    });

                    if(donors.length) {
                        this.saveNewMembers([donors[0].id]);
                    }
                }
            }).catch(res => {

            }).finally(() => {
                this.http.donors = false;
            });
        },
        removeMember(id) {
            this.form.members.splice(this.form.members.indexOf(id), 1);
        }
    }
});
