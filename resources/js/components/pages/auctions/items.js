import Vue from 'vue';
import AuctionItem from "../../../dtos/auction-item";
import draggable from 'vuedraggable';

Vue.component('auction-items', {
    components: {draggable},
    props: {
        items: {
            type: Array,
            required: true,
            default() {
                return [];
            }
        },
        auction: {
            type: Object,
            required: true
        }
    },
    data() {
        return {
            http: {
                items: false,
                order: false
            },
            mItems: []
        };
    },
    created() {
        this.transformItems(this.items);
    },
    methods: {
        transformItems(items) {
            this.mItems = items.map(i => {
                return new AuctionItem(i);
            });
        },
        getItems() {
            this.http.items = true;

            this.$http.get(`/ajax/auctions/${this.auction.id}/auction-items`).then(res => {
                this.transformItems(res.data.data);
            }).catch(res => {

            }).finally(() => {
                this.http.items = false;
            });
        },
        updateItemNumbers(ids) {
            let data = {ids};
            this.http.order = true;

            this.$http.post(`/ajax/auctions/${this.auction.id}/item-numbers-order`, data).then(res => {
                this.getItems();
            }).catch(res => {

            }).finally(() => {
                this.http.order = false;
            });
        },
        listUpdated({idList}) {
            this.updateItemNumbers(idList);
        }
    }
});
