import Vue from 'vue';
import _ from 'lodash';

Vue.component('auction-vendors', {
    props: {
        statuses: {
            type: Array,
            default() {
                return [];
            }
        },
        vendors: {
            type: Array,
            default() {
                return [];
            }
        },
        auction: {
            type: Object,
            required: true
        }
    },
    data() {
        return {
            http: {
                vendors: false
            },
            selectedVendor: {},
            vendorStatus: [],
            query: null,
            vendorData: this.vendors
        }
    },
    created() {
        this.$bus.$on('auction-vendors:updated', ({auction_id, vendor_id}) => {
            this.fetchVendors();
        });
    },
    computed: {
        mVendors() {
            return this.vendorData.filter(v => {
                if(!this.vendorStatus || !this.vendorStatus.length) {
                    return true;
                }

                let index = this.vendorStatus.indexOf(_.get(v, 'status.id', null));

                return index > -1;
            }).filter(v => {
                if(!this.query) {
                    return true;
                }

                return v.name.toLowerCase().includes(this.query.toLowerCase());
            });
        },
        vendorsAreFiltered() {
            return this.query || (this.vendorStatus && this.vendorStatus.length);
        }
    },
    methods: {
        editStatus(vendor, cb) {
            this.selectedVendor = vendor;

            cb();
        },
        vendorBgStatusSlug(vendor) {
            if(!vendor.status) {
                return '';
            }

            return `bg-nsca-${vendor.status.slug}`;
        },
        fetchVendors() {
            this.http.vendors = true;

            this.$http.get(`/ajax/auctions/${this.auction.id}/vendors`).then(res => {
                this.vendorData = res.data.data;
            }).catch(res => {
                console.log(res.response || null);
            }).finally(() => {
                this.http.vendors = false;
            });
        },
        vendorEditUrl(url) {
            return `${url}?redirect-to=${window.location.pathname}`
        }
    }
});
