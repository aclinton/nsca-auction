import Vue from 'vue';
import './bootstrap';

const app = new Vue({
    el: '#app',
    mounted() {
        this.$bus.$on('modal:open', (data) => {
            let modalId = data.modal;

            $(document).ready(() => {
                $(`${modalId}`).modal('show');
            });
        });

        this.$bus.$on('modal:close', (data) => {
            let modalId = data.modal;

            $(document).ready(() => {
                $(`${modalId}`).modal('hide');
            });
        });

        $(document).ready(() => {
            $('[data-toggle="tooltip"]').tooltip()
        });
    }
});
