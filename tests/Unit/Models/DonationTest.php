<?php

namespace Tests\Unit\Models;

use App\Models\AuctionItem;
use App\Models\Donation;
use App\Models\Donor;
use App\Models\Vendor;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class DonationTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function has_auction()
    {
        /** @var Donation $donation */
        $donation = factory(Donation::class)->create();

        $this->assertNotNull($donation->auction);
    }

    /** @test */
    public function has_type()
    {
        /** @var Donation $donation */
        $donation = factory(Donation::class)->create();

        $this->assertNotNull($donation->type);
    }

    /** @test */
    public function has_regular_donor()
    {
        /** @var Donation $donation */
        $donation = factory(Donation::class)->create();
        /** @var Donor $donor */
        $donor = factory(Donor::class)->create();

        $donation->memberDonors()->attach($donor);

        $this->assertEquals($donor->id, $donation->refresh()->memberDonors()->first()->id);
    }

    /** @test */
    public function has_multiple_regular_donors()
    {
        /** @var Donation $donation */
        $donation = factory(Donation::class)->create();
        /** @var Donor $donor */
        $donor = factory(Donor::class)->create();
        /** @var Donor $donor2 */
        $donor2 = factory(Donor::class)->create();

        $donation->memberDonors()->attach([$donor->id, $donor2->id]);
        $donation->refresh();

        $this->assertEquals($donation->memberDonors()->count(), 2);
        $this->assertDatabaseHas('donation_donor', [
            'donor_id' => $donor->id
        ]);
        $this->assertDatabaseHas('donation_donor', [
            'donor_id' => $donor2->id
        ]);
    }

    /** @test */
    public function has_vendor_donor()
    {
        /** @var Donation $donation */
        $donation = factory(Donation::class)->create();
        /** @var Vendor $donor */
        $vendor = factory(Vendor::class)->create();

        $donation->vendorDonors()->attach($vendor);

        $this->assertEquals($vendor->id, $donation->refresh()->vendorDonors()->first()->id);
    }

    /** @test */
    public function has_multiple_vendor_donors()
    {
        /** @var Donation $donation */
        $donation = factory(Donation::class)->create();
        /** @var Vendor $donor */
        $donor = factory(Vendor::class)->create();
        /** @var Vendor $donor2 */
        $donor2 = factory(Vendor::class)->create();

        $donation->vendorDonors()->attach([$donor->id, $donor2->id]);
        $donation->refresh();

        $this->assertEquals($donation->vendorDonors()->count(), 2);
        $this->assertDatabaseHas('donation_donor', [
            'donor_id' => $donor->id
        ]);
        $this->assertDatabaseHas('donation_donor', [
            'donor_id' => $donor2->id
        ]);
    }

    /** @test */
    public function has_multiple_auction_items()
    {
        /** @var Donation $donation */
        $donation = factory(Donation::class)->create();
        /** @var Donation $donation2 */
        $donation2 = factory(Donation::class)->create();
        /** @var AuctionItem $auction_item */
        $auction_item = factory(AuctionItem::class)->create();

        $donation->auctionItems()->attach($auction_item->id, ['qty' => $donation->qty - 1]);
        $donation2->auctionItems()->attach($auction_item->id, ['qty' => $donation2->qty - 1]);

        $this->assertEquals($donation->auctionItems()->count(), 1);
        $this->assertEquals($donation2->auctionItems()->count(), 1);
    }

    /** @test */
    public function has_auction_item_with_qty()
    {
        /** @var Donation $donation */
        $donation = factory(Donation::class)->create();
        /** @var AuctionItem $auction_item */
        $auction_item = factory(AuctionItem::class)->create();

        $donation->auctionItems()->attach($auction_item->id, ['qty' => $donation->qty - 1]);
        $this->assertEquals($donation->auctionItems()->first()->pivot->qty, $donation->qty - 1);
    }
}
