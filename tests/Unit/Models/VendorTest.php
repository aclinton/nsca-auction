<?php

namespace Tests\Unit\Models;

use App\Models\Auction;
use App\Models\Vendor;
use App\Models\VendorStatus;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class VendorTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function has_auctions()
    {
        /** @var Vendor $vendor */
        $vendor = factory(Vendor::class)->create();
        /** @var Auction $auction */
        $auction = factory(Auction::class)->create();

        $vendor->auctions()->attach($auction->id);

        $this->assertEquals($vendor->refresh()->auctions()->count(), 1);
    }

    /** @test */
    public function has_auction_status()
    {
        /** @var Vendor $vendor */
        $vendor = factory(Vendor::class)->create();
        /** @var Auction $auction */
        $auction = factory(Auction::class)->create();
        /** @var VendorStatus $status */
        $status = factory(VendorStatus::class)->create();

        $vendor->auctions()->attach($auction->id, ['vendor_status_id' => $status->id]);
        $pivot = $vendor->refresh()->auctions()->first()->pivot;

        $this->assertEquals($pivot->status->id, $status->id);
    }
}
