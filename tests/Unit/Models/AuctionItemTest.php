<?php

namespace Tests\Unit\Models;

use App\Models\AuctionItem;
use App\Models\Donation;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class AuctionItemTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function has_auction()
    {
        /** @var AuctionItem $auction_item */
        $auction_item = factory(AuctionItem::class)->create();

        $this->assertNotNull($auction_item->auction);
    }

    /** @test */
    public function has_type()
    {
        /** @var AuctionItem $auction_item */
        $auction_item = factory(AuctionItem::class)->create();

        $this->assertNotNull($auction_item->type);
    }

    /** @test */
    public function has_a_single_donation()
    {
        /** @var AuctionItem $auction_item */
        $auction_item = factory(AuctionItem::class)->create();
        /** @var Donation $donation */
        $donation = factory(Donation::class)->create();

        $auction_item->donations()->attach($donation->id, ['qty' => $donation->qty - 1]);

        $this->assertTrue($donation->is($auction_item->refresh()->donations()->first()));
    }
}
