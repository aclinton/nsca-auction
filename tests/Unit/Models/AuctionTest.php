<?php

namespace Tests\Unit\Models;

use App\Models\Auction;
use App\Models\Vendor;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class AuctionTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function has_vendors()
    {
        /** @var Vendor $vendor */
        $vendor  = factory(Vendor::class)->create();
        /** @var Auction $auction */
        $auction = factory(Auction::class)->create();

        $auction->vendors()->attach($vendor->id);

        $this->assertEquals($auction->refresh()->vendors()->count(), 1);
    }
}
