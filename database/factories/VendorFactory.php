<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\Vendor::class, function (Faker $faker) {
    return [
        'name'         => $faker->company,
        'url'          => $faker->url,
        'email'        => $faker->companyEmail,
        'phone'        => $faker->phoneNumber,
        'contact_name' => $faker->name,
        'address'      => $faker->address,
        'city'         => $faker->city,
        'zip'          => $faker->postcode,
        'state'        => $faker->state,
    ];
});
