<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$statuses = ['contacted', 'participating', 'declined'];

$factory->define(\App\Models\VendorStatus::class, function (Faker $faker) use($statuses) {
    return [
        'name' => $statuses[array_rand($statuses)]
    ];
});
