<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\Donation::class, function (Faker $faker) {
    return [
        'donation_type_id' => factory(\App\Models\DonationType::class),
        'auction_id'       => factory(\App\Models\Auction::class),
        'accepted'         => $faker->boolean,
        'name'             => $faker->word,
        'description'      => $faker->sentence,
        'qty'              => $faker->numberBetween(1, 10),
        'value'            => $faker->numberBetween(0, 150)
    ];
});
