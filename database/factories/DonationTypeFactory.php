<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$donation_types = ['good', 'service'];

$factory->define(\App\Models\DonationType::class, function (Faker $faker) use($donation_types) {
    return [
        'name' => $donation_types[array_rand($donation_types)]
    ];
});
