<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\Auction::class, function (Faker $faker) {
    return [
        'year' => $faker->year
    ];
});
