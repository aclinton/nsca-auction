<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$types = ['live', 'silent'];

$factory->define(\App\Models\AuctionItemType::class, function (Faker $faker) use($types) {
    return [
        'name' => $types[array_rand($types)]
    ];
});
