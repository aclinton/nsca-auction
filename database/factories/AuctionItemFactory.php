<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\AuctionItem::class, function (Faker $faker) {
    return [
        'name'                 => $faker->word,
        'description'          => $faker->sentence,
        'auction_id'           => factory(\App\Models\Auction::class),
        'auction_item_type_id' => factory(\App\Models\AuctionItemType::class)
    ];
});
