<?php

use Illuminate\Database\Seeder;

class AuctionItemTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = config('auction_item_types');

        foreach($types as $type) {
            $_t = \App\Models\AuctionItemType::firstOrCreate(['name' => $type]);

            $this->command->info("Seeded: {$_t->name}");
        }
    }
}
