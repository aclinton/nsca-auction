<?php

use App\Services\PermissionService;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RolePermissionsSeeder extends Seeder
{
    /**
     * @var array
     */
    protected $map = [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        foreach (config('roles') as $role => $config) {
            /** @var Role $role_object */
            $role_object = Role::whereName($role)->first();

            if(!$role_object) {
                $this->command->error("No role exists: " . $role);
                continue;
            }

            \DB::beginTransaction();

            $this->command->info("> building permissions for " . $role);

            foreach($config['permissions'] ?? [] as $resource => $permission) {
                if($permission == '*') {
                    $permissions = PermissionService::getResourcePermissions($resource);
                } else {
                    foreach($permission as $perm) {
                        $permissions[] = PermissionService::getResourceAction($resource, $perm);
                    }
                }

                $this->command->info("> attaching $resource permissions: " . implode(', ', $permissions ?? []));
                $role_object->givePermissionTo($permissions ?? []);
                $this->command->getOutput()->newLine();
                $permissions = null;
            }

            \DB::commit();
        }
    }
}
