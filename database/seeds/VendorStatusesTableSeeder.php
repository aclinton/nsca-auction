<?php

use Illuminate\Database\Seeder;

class VendorStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = config('auction_vendor_statuses');

        foreach($statuses as $status) {
            $s = \App\Models\VendorStatus::firstOrCreate([
                'name' => $status
            ]);

            $this->command->info('Seeded -> ' . $s->name);
        }
    }
}
