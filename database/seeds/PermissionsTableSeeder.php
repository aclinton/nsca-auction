<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * @var array
     */
    protected $map = [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $truncate = $this->command->confirm('Truncate existing permissions?');

        if($truncate) {
            \Spatie\Permission\Models\Permission::query()->delete();
            $this->command->info("> Cleared existing permissions");
        }

        foreach (config('permissions.resources') as $resource => $config) {
            foreach ($config['crud'] ?? [] as $action) {
                $mapped_action = $this->map[$action];
                $name          = "$resource.$mapped_action";

                $permission = \Spatie\Permission\Models\Permission::firstOrCreate([
                    'name' => $name
                ]);

                if($permission->wasRecentlyCreated) {
                    $this->command->info("> Permission created: " . $permission->name);
                }
            }

            foreach ($config['custom'] ?? [] as $action) {
                $name = "$resource.$action";

                $permission = \Spatie\Permission\Models\Permission::firstOrCreate([
                    'name' => $name
                ]);

                if($permission->wasRecentlyCreated) {
                    $this->command->info("> Permission created: " . $permission->name);
                }
            }
        }
    }
}
