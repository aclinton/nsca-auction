<?php

use Illuminate\Database\Seeder;

class DonationTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = config('donation_types');

        foreach($types as $type) {
            $_t = \App\Models\DonationType::firstOrCreate([
                'name' => $type
            ]);

            $this->command->info("Seeded: {$_t->name}");
        }
    }
}
