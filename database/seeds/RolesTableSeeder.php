<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (config('roles') as $name => $config) {
            $role = \Spatie\Permission\Models\Role::firstOrCreate(['name' => $name], [
                'name'       => $name,
                'guard_name' => $config['guard'] ?? null
            ]);

            if ($role->wasRecentlyCreated) {
                $this->command->info('> Role created: ' . $role->name);
            }
        }
    }
}
