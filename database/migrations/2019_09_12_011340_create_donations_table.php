<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('donation_type_id')->nullable();
            $table->unsignedInteger('auction_id')->nullable();
            $table->boolean('accepted')->nullable();
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->integer('qty')->nullable();
            $table->decimal('value')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('auction_id')->references('id')->on('auctions')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('donation_type_id')->references('id')->on('donation_types')->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donations');
    }
}
