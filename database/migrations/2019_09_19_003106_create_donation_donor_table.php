<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonationDonorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donation_donor', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('donation_id')->nullable();
            $table->unsignedInteger('donor_id')->nullable();
            $table->string('donor_type')->nullable();
            $table->index(['donor_id', 'donor_type']);
            $table->index(['donor_id', 'donor_type', 'donation_id']);
            $table->timestamps();

            $table->foreign('donation_id')->references('id')->on('donations')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donation_donor');
    }
}
