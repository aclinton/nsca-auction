<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFamilyIdToAuctionVendor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auction_vendor', function (Blueprint $table) {
            $table->unsignedInteger('school_family_id')->nullable()->after('vendor_status_id');

            $table->foreign('school_family_id')->references('id')->on('school_families')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auction_vendor', function (Blueprint $table) {
            $table->dropForeign('auction_vendor_school_family_id_foreign');
            $table->dropColumn('school_family_id');
        });
    }
}
