<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInviteRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invite_role', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('role_id')->nullable();
            $table->unsignedInteger('invite_id')->nullable();
            $table->timestamps();

            $table->index(['role_id', 'invite_id']);
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('invite_id')->references('id')->on('invites')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invite_role');
    }
}
