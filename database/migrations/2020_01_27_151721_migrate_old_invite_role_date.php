<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MigrateOldInviteRoleDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $invites = \App\Models\Invite::whereHas('role')->get();

        foreach($invites as $invite) {
            $role = $invite->role;

            if(!$invite->roles->contains($role->id)) {
                $invite->roles()->attach($role);
            }
        }

        Schema::table('invites', function (Blueprint $table) {
            $table->dropForeign('invites_role_id_foreign');
            $table->dropColumn('role_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invites', function (Blueprint $table) {
            $table->unsignedBigInteger('role_id')->nullable()->after('id');
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('set null')->onUpdate('cascade');
        });
    }
}
