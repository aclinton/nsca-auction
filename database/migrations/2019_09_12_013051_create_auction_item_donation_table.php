<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuctionItemDonationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auction_item_donation', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('auction_item_id')->nullable();
            $table->unsignedInteger('donation_id')->nullable();
            $table->integer('qty')->nullable();
            $table->index(['auction_item_id', 'donation_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auction_item_donation');
    }
}
