<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuctionVendorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auction_vendor', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('auction_id')->nullable();
            $table->unsignedInteger('vendor_id')->nullable();
            $table->unsignedInteger('vendor_status_id')->nullable();
            $table->index(['auction_id', 'vendor_id']);
            $table->timestamps();
            $table->foreign('vendor_status_id')->references('id')->on('vendor_statuses')->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auction_vendor');
    }
}
