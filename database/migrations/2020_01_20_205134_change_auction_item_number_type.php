<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeAuctionItemNumberType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auction_items', function (Blueprint $table) {
            $table->dropIndex('auction_items_item_number_index');
            $table->dropColumn('item_number');
        });

        Schema::table('auction_items', function (Blueprint $table) {
            $table->unsignedInteger('item_number')->after('id')->index()->nullable();
            $table->index(['item_number', 'auction_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
