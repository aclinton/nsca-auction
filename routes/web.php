<?php

Route::get('/', 'WelcomeController');

Auth::routes(['register' => false]);

Route::group(['middleware' => ['auth']], function () {
    Route::impersonate();
});

# Public routes
Route::group(['prefix' => 'auction-vendors/{auction_code}', 'middleware' => ['auction.code_exists']], function () {
    Route::get('/', 'PublicAuctionVendorsController');
});

# Admin routes
Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function () {
    Route::resource('activities', 'Admin\ActivitiesController', ['as' => 'admin'])
        ->only('index');
    Route::resource('users', 'Admin\UsersController', ['as' => 'admin'])
        ->only('index', 'edit', 'update');
    Route::resource('roles', 'Admin\RolesController', ['as' => 'admin'])
        ->only('index');
    Route::resource('invites', 'Admin\InvitesController', ['as' => 'admin'])
        ->only('index', 'create', 'store');
    Route::post('invites/{invite}/resend', 'Admin\SendInvitesController')
        ->name('resend-invite')
        ->middleware(['can:invites.resend']);
});

# Main application routes including ajax
Route::group(['middleware' => ['auth', 'page-view.log']], function () {
    Route::resource('auctions', 'AuctionsController')
        ->only('index', 'create', 'store');
    Route::resource('donors', 'DonorsController')
        ->only('index', 'create', 'store', 'edit', 'update');
    Route::resource('school-families', 'SchoolFamiliesController')
        ->only('index', 'create', 'store', 'edit', 'update');
    Route::resource('vendors', 'VendorsController')
        ->only('index', 'create', 'store', 'edit', 'update');
    Route::apiResource('vendor-imports', 'VendorFileImportsController')
        ->only('store');

    # Nested auction routes
    Route::resource('auctions.auction-items', 'AuctionItemsController')
        ->only('index', 'create', 'store', 'edit', 'update');
    Route::resource('auctions.donations', 'AuctionDonationsController')
        ->only('index', 'create', 'store', 'edit', 'update');
    Route::resource('auctions.school-families', 'AuctionSchoolFamiliesController')
        ->only('index');
    Route::resource('auctions.vendors', 'AuctionVendorsController')
        ->only('index');

    # Single action endpoints
    Route::post('donations/{donation}/approve', 'AuctionDonationApprovalsController')
        ->name('donation-approve')
        ->middleware('can:auction.donations.approve');
    Route::post('donations/{donation}/reject', 'AuctionDonationRejectsController')
        ->name('donation-reject')
        ->middleware('can:auction.donations.approve');

    # Ajax routes
    Route::group(['prefix' => 'ajax'], function () {
        Route::apiResource('auctions.vendor-registrations', 'Ajax\AuctionVendorRegistrationsController')
            ->only('store');
        Route::apiResource('auctions.vendors', 'Ajax\AuctionVendorsController', [
            'as' => 'ajax'
        ])
            ->only('update', 'index');
        Route::apiResource('auctions.auction-items', 'Ajax\AuctionItemsController', [
            'as' => 'ajax'
        ])
            ->only('index');
        Route::apiResource('donors', 'Ajax\DonorsController', [
            'as' => 'ajax'
        ])
            ->only('index', 'store');
        Route::post('auctions/{auction}/item-numbers-order', 'Ajax\AuctionItemNumbersOrderController', [
            'as' => 'ajax',
        ])->middleware(['can:auction.auction-items.reorder']);
    });
});

# Invitation routes
Route::group(['prefix' => 'invites/{token}/accept', 'middleware' => ['invite.valid', 'page-view.log']], function () {
    Route::get('/', 'InviteAcceptsController@create')->name('invite.view');
    Route::post('/', 'InviteAcceptsController@store')->name('invite.accept');
});
